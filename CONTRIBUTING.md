# Contributing 

Although we are always happy to make improvements ourselves, we also
welcome changes and improvements from the community!

Have a fix for a problem you've been running into or an idea for a new feature
you think would be useful? Here's what you need to do:

1. [Read and understand the Code of Conduct](#code-of-conduct).
1. Fork this repo and clone your fork to somewhere on your machine.
1. [Ensure that you have a working environment](#setting-up-your-environment).
1. Read up on the [architecture of the project](#architecture), [how to run
   tests](#running-tests), and [the code style we use in this
   project](#code-style).
1. Cut a new branch and start work on the feature or bugfix you plan
   on implementing.
1. [Make sure your branch is well managed as you go
   along](#managing-your-branch).
1. Push to your fork and submit a pull request.
1. [Ensure that the test suite passes on Travis and make any necessary changes
   to your branch to bring it to green.](#continuous-integration)

Although we maintain this application in our free time, we try to respond to
contributions in a timely manner. Once we look at your pull request, we may give
you feedback. For instance, we may suggest some changes to make to your code to
fit within the project style or discuss alternate ways of addressing the issue
in question. Assuming we're happy with everything, we'll then bring your changes
into master. Now you're a contributor!

---

## Code of Conduct

If this is your first time contributing, please read the ShieldsUp [Code of Conduct]. We
want to create a space in which everyone is allowed to contribute, and we
enforce the policies outlined in this document.

[Code of Conduct]: http://wiki.shieldsup.org.nz/index.php?title=Code_of_Conduct

## Setting up your environment

There are some simple steps to setting up a development enivronment outlined in
the [README](/README.md).

## Architecture

This project follows the typical structure for a Django project: code is located in `shieldsup`. Discrete parts of the application are located in subdirectories underneath the top-level, with a `shieldsup` application binding them all together.

Generally, we want to try and push Django's built in administration UI
as far as we can before we start adding our own UI and alternative views. Please
keep this in mind as you implement a feature, as introducing new UI frameworks
or styles at this point may become a burden in the future and slow development.

To start gaining an understanding of the project structure, start with each application's `urls.py` to get a feel for the available routes. From there, the 
`models.py` are also worth a read to understand the data model.

### Tests

Tests are broken up into two categories:

* Unit tests — low-level tests for individual classes (you're probably
  interested in these). These tests typically stub out external dependencies 
  such as third-party APIs. Unit tests should be written within the Django
  testing framework.
* End-to-end system tests — high-level tests to ensure that the application works for some selected, core workflows. These tests are not updated frequently but are important to make sure that minor changes do not affect the functionality of the application. We're still determining the most appropriate testing framework for 
end-to-end tests.

Our approach to testing tends to iterate over time. The best approach for writing tests is to copy an existing test in the same file as where you want to add a new test. We may suggest changes to bring the tests in line with 
our current approach.

## Code style

We largely follow the style rules laid down within the PEP8 guide, enforced by
[pylint](https://www.pylint.org/). We do make some adjustments to the recommendations, the configuration for which you can find in the `.pylintrc` file 
in the root of the project, however here are
some key differences:

* We allow code to be similar up to 10 lines before warning. This is to prevent naturally similar code such as data model migrations from falsely-warning.
* We have increased the maximum line length to 120 characters.
* We have disabled format checks in a handful of places. Mostly, these places are 
  management scripts where a high local variable count leads to a better understanding of how the script functions.

## Running tests

### Unit tests

Unit tests are the most common kind of tests in the application (although that's not saying much). They exercise code file by file using either local objects or
objects temporarily stored in the database using transactions.

To run a single module's unit test, you might say something like:

```bash
./manage.py test users.tests
```

You can run all unit tests (fast) as well:

```bash
./manage.py test
```

### End-to-end tests

The end to end tests exercise matchers using an actual web browser. We aim to 
select the most common & important workflows within the application to exercise
that tests correctly catch pass and fail conditions.

We're still investigating the best tool for creating end to end tests. 
It's likely to be something based on Selenium and chromedriver.

If you are an experienced tester, feel free to contribute end to end tests! 

This document will be updated when we know more about end-to-end testing. If you
have found end-to-end tests, and this documentation was not up to date, please [open an issue](/issues/new).

## Managing your branch

* Use well-crafted commit messages, providing context if possible.
* Squash "WIP" commits and remove merge commits by rebasing your branch against
  `master`. We try to keep our commit history as clean as possible, but don't 
  insist upon rebasing unless the history is difficult to follow.

## Documentation

As you navigate the codebase, you may notice certain classes and methods that
are prefaced with inline documentation. 

If your changes end up extending or updating the interface between modules, it helps greatly to update the 
docs at the same time for future developers and other readers of the source code.

If you write some code that you think may be hard for others to understand or follow, first consider whether it can be refactored to be made simpler or clearer. If it cannot, then add some code comments explaning the change. 


## Continuous integration

While running the tests is a great way to check your work, this command
will only run against your local build, and not a potential release build. 
We also run additional checks in CI that may catch things missed locally, including linting and package audits.

Ultimately, though, we want to ensure that your changes work the way you expect them to. We make use of [Gitlab's CI tool][gitlab-ci] to do this work for us. Gitlab
will kick in after you push up a branch or open a PR. It takes a few minutes to
run a complete build, which you are free to
monitor as it progresses.

[gitlab-ci]: https://gitlab.com/shieldsupnz/shieldsup-django/pipelines

What happens if the build fails in some way? Don't fear! Click on a failed job
and scroll through its output to determine the cause of the failure. You'll want
to make changes to your branch and push them up until the entire build is green.
It may take a bit of time, but overall it is worth it and it helps us immensely!

