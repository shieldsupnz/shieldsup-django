# ShieldsUp

## Purpose

This application exists to coordinate the production of
face masks via the maker community, and connect with those
who require masks. 

This coordination is achieved using a Django application, with
all coordination behaviour currently implemented using the Django admin 
contrib module. We have plans in the roadmap to add API endpoints for 
core workflows to support other applications.

This application is anticipated to receive moderate traffic from the NZ
maker community. Scalability is not our immediate goal - rather, we aim
to support the needs of our local, national and international community.

## Operations:

### Repository management

All deployments should be performed by CI unless there are specific requirements or dependencies which prevent this.

* **master**
  * The default branch features are merged into.
  * master should always have a green build.
  * Should always deploy to staging environment if one exists.
  * feature/ and bugfix/ work will be branched from, and merged back into, master.
* **feature/xxx-yyy**
  * Development on new features happens in feature branches. 
  * Branch names should contain the issue number and a brief description of the feature. 
  * For example feature/753-widget-show-page.
* **bugfix/xxx-yyy**
  * Bug fixes should happen in branches. 
  * Branch names should contain the issue number and a brief description of the bug. 
  * For example bugfix/23-correct-pluralisation-of-radius.

### Environments

| **Environment** | **URL** | **Hosting Platform**                 | **Git Branch** | **Exception monitoring URL** | **Logs available at**                                            |
| --------------- | ------- | ------------------------------------ | -------------- | ---------------------------- | ---------------------------------------------------------------- |
| Production      | app.shieldsup.org.nz    | Docker Compose on DigitalOcean | production     | Sentry       | On-disk |


### Browser Support

| **Browser**            | **Supported Versions** |
| ---------------------- | ---------------------- |
| Internet Explorer      | 11+                    |
| Edge                   | 17+                    |
| Safari                 | Latest -2              |
| Chrome                 | Latest -2              |
| Firefox                | Latest -2              |
| iOS Safari             | Latest -2              |
| Chrome for Android     | Latest -2              |
| UC Browser for Android | Latest -2              |
| Samsung Internet       | Latest -2              |

Broken UI or behaviour on the browers aboved can be considered a bug. Other browsers 
and platform bugs will be considered based on the scope & severity of the bug and fix.

## Secrets

All credentials and the `.env` file for this project are stored and managed by Rohan Fletcher.
If you are making a change that requires configuration to be added, removed or changed in deployed
enironments, please _make this clear in your merge request_ so that the release can be coordinated
properly.

## Project Resources:

| **Resource**    | **URL**                           |
| --------------- | --------------------------------- |
| Development Board     | https://gitlab.com/shieldsupnz/shieldsup-django/-/boards                              |
| CI URL          | https://gitlab.com/shieldsupnz/shieldsup-django/pipelines                              |
| ShieldsUp website | https://shieldsup.org.nz/ |
| Contribution guidelines | [CONTRIBUTING.md](/CONTRIBUTING.md) |
| Code of conduct | http://wiki.shieldsup.org.nz/index.php?title=Code_of_Conduct |


## People Involved

See [`CONTRIBUTORS.md`](/CONTRIBUTORS.md)


## Comms:

* The best way of communicating in real time is to join the `#webapp` channel on the ShieldsUp slack.
* Issues are also an appropriate communication mechanism for enquiries about new features or potential bugs/issues.
* Please avoid direct communication such as email and DM - it's easier for others to see what's happening with the project if 
  as much discussion as possible happens in the open.


## Developers

### Dependencies

Before running this app and its tests locally you will need:

1. SQLite
2. Library headers for the above (`libsqlite3-dev`, `libpq-dev`).
3. pipenv (NOTE: do not install this from OS packages, as these are frequently outdated.)
4. Python3

### Running the app

1. Clone the project: `git clone https://gitlab.com/shieldsupnz/shieldsup-django.git`
2. Enter the Django project - `cd shieldsup-django/sheildsup`
3. Set up the pipenv - `pipenv install`
4. Set up your shell - `pipenv shell`
5. Run migrations: `./manage.py migrate`
6. Create a super user: `./manage.py createsuperuser`
7. Run the server: `./manage.py runserver`

### Running the tests

`CONTRIBUTING.md` includes more information on the structure and types
of tests. Core check commands are listed below:

```
pipenv run test
pipenv run lint
pipenv check
```

### Management tasks

Currently there are 3 management commands:
- **remotedatapull** (deprecated) - Used to pull in data from the original airtable database
- **createusersfromcsv** - Creates users and attached manufacturers in bulk from a csv input and outputs account details
- **rqscheduler** - Starts a scheduler for worker jobs (rq / redis based). At present there are no jobs scheduled.

## Documentation:

  The project wiki in the resources table above contains useful
  general information about the project. Inline comments and documentation
  in the source code for this project should also be useful. 

  If you have looked for and failed to find documentation on something, please
  ask on Slack or open an issue on Gitlab.

## Running in production

### Infrastructure

There are multi-environment terraform definitions and secrets that have been set up through a tool called terrabuild, 
but some work needs to be done to remove secrets before they are released.

### security.txt

* `/.well-known/security.txt` exists with instructions which security researchers can use to contact us if they find an issue in this application.

### Logging

* This application logs to STDOUT which is captured by `docker-compose`.
* Logs are also saved to disk.

### Exception monitoring

* Exceptions are tracked using Sentry
* Access to Sentry can be granted as required, please ask on Slack.
* Where possible, add Sentry data to a corresponding Gitlab ticket.
