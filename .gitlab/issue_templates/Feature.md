<!-- The first three sections: "Problem to solve", "Intended users" and "Proposal", are strongly recommended, while the rest of the sections can be filled out during the problem validation or breakdown phase. However, keep in mind that providing complete and relevant information early helps our product team validate the problem and start working on a solution. -->

### Problem to solve

<!-- What problem should we solve? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." -->

### Intended users

<!-- Who will use this feature? If known, include any of the following: types of users (e.g. Maker, Distributor, Hub Coordinator), or specific roles (e.g. WLG Hub Manager). It's okay to write "Unknown" and fill this field in later.

### Further details

<!-- Include use cases, benefits, goals, or any other details that will help us understand the problem better. -->

### Proposal

<!-- 
  Do you have any ideas about how to solve this problem? Please include as much information
  as possible, including links or screenshots of the type of feature you've seen elsewhere that could
  work well.
-->

<!-- How are we going to solve the problem? Try to include the user journey! https://about.gitlab.com/handbook/journeys/#user-journey -->

### Permissions and Security

<!-- What permissions are required to perform the described actions? Should a limited group of users
be able to access and/or use the feature? Do they fit into existing user roles we have established?-->

### What does success look like, and how can we measure that?

<!-- Define both the success metrics and acceptance criteria. Note that success metrics indicate the desired outcomes for the whole ShieldsUp platform, while acceptance criteria indicate when the specific solution is working correctly. -->

### Links / references

/label ~feature
