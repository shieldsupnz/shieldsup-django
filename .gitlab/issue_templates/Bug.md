
<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label:

- https://gitlab.com/shieldsupnz/shieldsupnz-django/issues?label_name%5B%5D=regression
- https://gitlab.com/shieldsupnz/shieldsupnz-django/issues?label_name%5B%5D=bug

and verify the issue you're about to submit isn't a duplicate.

--->

### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)


### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Relevant screenshots

(Paste any relevant screenshots of the issue occurring)

#### Browser details

Please provide details of your operating system or browser here.
If you are unsure of what your browser details are, https://www.whatsmybrowser.org/ will
generate a link you can copy and paste here with all the necessary information.


### Possible fixes

(If you are technically inclined, link to the line of code that might be responsible for the problem)

/label ~bug
