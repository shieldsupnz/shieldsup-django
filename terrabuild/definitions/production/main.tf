
module "appserver" {
  source = "../common/modules/digitalocean"
  app_name = "${var.app_name}"
  deploy_namespace = "${var.deploy_namespace}"
  domain_name = "${var.domain_name}"
  deploy_user = "${var.deploy_user}"
  do_droplet_size = "s-1vcpu-1gb"
  authorized_keys_path = "${var.authorized_keys_path}"
  deploy_dir = "${var.deploy_dir}"
  digitalocean_api_token = "${var.digitalocean_api_token}"
  public_key_path = "${var.public_key_path}"
}

module "gitlab" {
  source = "../common/modules/gitlab"
  deploy_domain = "${module.appserver.fqdn}"
  deploy_namespace = "${var.deploy_namespace}"
  deploy_user = "${var.deploy_user}"
  deploy_dir = "${var.deploy_dir}"
  deploy_private_key_path = "${var.deploy_private_key_path}"
  host_public_keys = "${module.appserver.host_public_keys}"
  gitlab_api_token = "${var.gitlab_api_token}"
  gitlab_repo = "${var.gitlab_repo}"
}
