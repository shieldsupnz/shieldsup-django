# # Create a new domain
# resource "digitalocean_domain" "default" {
#   name = "${var.domain_name}"
# }

# # Add a record to the domain
# resource "digitalocean_record" "app" {
#   domain = "${digitalocean_domain.default.name}"
#   type   = "A"
#   name   = "app"
#   value  = "${digitalocean_droplet.app.ipv4_address}"
#   ttl = 60
# }

locals {
  app_fqdn = "app.${var.domain_name}" # quick workaround
  # app_fqdn = "${digitalocean_record.app.fqdn}"
}