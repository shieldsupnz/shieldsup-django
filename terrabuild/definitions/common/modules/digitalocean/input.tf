variable "do_droplet_image" {
  default = "ubuntu-18-04-x64"
}
variable "do_droplet_region" {
  default = "sfo2"
}
variable "do_droplet_size" {
  default = "s-1vcpu-1gb"
}
variable "app_name" {}

variable "domain_name" {}
variable "authorized_keys_path" {}
variable "deploy_user" {}
variable "deploy_dir" {}
variable "deploy_namespace" {}

variable "digitalocean_api_token" {
  description = "The token needed to talk to the DigitalOcean API"
}

variable "public_key_path" {
  description = "Public key to be added to newly created instance"
}