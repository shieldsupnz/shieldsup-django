
# Cloud-init - Render a part using a `template_file`
data "template_file" "cloud_config" {
  template = "${file("${path.module}/../../cloud_config.yaml")}"

  vars {
    authorized_keys = "${file(var.authorized_keys_path)}"
    deploy_user = "${var.deploy_user}"
    server_user = "${var.deploy_user}"
    deploy_dir = "${var.deploy_dir}"
  }
}

# Render a multi-part cloud-init config making use of the part
# above, and other source files
data "template_cloudinit_config" "default" {
  gzip          = false
  base64_encode = false

  # Main cloud-config configuration file.
  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.cloud_config.rendered}"
  }
}
