provider "digitalocean" {
  token = "${var.digitalocean_api_token}"
}

# load users public key outside to prevent duplicate key error
data "external" "ssh_key_fingerprint" {
  program = [
    "${path.module}/helpers/ssh-keygen-fingerprint",
    "${file(pathexpand("${var.public_key_path}"))}"
  ]
}

resource "digitalocean_droplet" "app" {
  image     = "${var.do_droplet_image}"
  name      = "${var.app_name}-${var.deploy_namespace}"
  region    = "${var.do_droplet_region}"
  size      = "${var.do_droplet_size}"
  user_data = "${data.template_cloudinit_config.default.rendered}"
  ssh_keys = ["${data.external.ssh_key_fingerprint.result.md5}"]

  connection {
    type = "ssh"
    user = "${var.deploy_user}"
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'Waiting for cloud-init to finish'... && cloud-init status --wait 2>&1 >/dev/null"
    ]
  }
}

data "external" "host_public_keys" {
  program = [
    "${path.module}/helpers/ssh-keyscan-wrapper",
    "${local.app_fqdn}",
    "${digitalocean_droplet.app.ipv4_address}"
  ]
}

resource "null_resource" "fix_known_hosts" {
  # Changes to any instance of the cluster requires re-provisioning
  provisioner "local-exec" {
    # automatically add the hosts public keys to known_hosts
    # this is ok because we are using public key authentication
    # see here: https://www.gremwell.com/ssh-mitm-public-key-authentication
    command = <<EOT
      ssh-keygen -R "${local.app_fqdn}";
      ssh-keygen -R "${digitalocean_droplet.app.ipv4_address}";
      ssh-keygen -R "${local.app_fqdn},${digitalocean_droplet.app.ipv4_address}";
EOT
  }

  provisioner "local-exec" {
    # Bootstrap script called with private_ip of each node in the clutser
    command = <<EOT
      echo "${data.external.host_public_keys.result.host_public_keys}" >> ${pathexpand("~/.ssh/known_hosts")};
EOT
  }
}
