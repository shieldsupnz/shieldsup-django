resource "digitalocean_firewall" "app" {
  name = "${var.app_name}-${var.deploy_namespace}-only-22-80-and-443"

  droplet_ids = ["${digitalocean_droplet.app.id}"]

  inbound_rule = [
    {
      protocol           = "tcp"
      port_range         = "22"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "tcp"
      port_range         = "80"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "tcp"
      port_range         = "443"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "icmp"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
  ]

  outbound_rule = [
  {
    protocol                = "tcp"
    port_range              = "53"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  },
  {
    protocol                = "udp"
    port_range              = "53"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  },
  {
    protocol                = "icmp"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  },
  {
    protocol           = "tcp"
    port_range         = "80"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  },
  {
    protocol           = "tcp"
    port_range         = "443"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  },
  {
    protocol           = "udp"
    port_range         = "123"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  },
]
}
