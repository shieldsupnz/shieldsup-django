output "fqdn" {
  value = "${local.app_fqdn}"
}

output "ip" {
  value = "${digitalocean_droplet.app.ipv4_address}"
}

output "cloud_config" {
  value = "${data.template_file.cloud_config.rendered}"
  sensitive = true
}

output "ssh" {
  value = "${data.template_file.cloud_config.vars.server_user}@${digitalocean_droplet.app.ipv4_address}"
}

output "host_public_keys" {
  value = "${data.external.host_public_keys.result["host_public_keys"]}"
  sensitive = true
}
