
variable "gitlab_domain" {
  default = "gitlab.com"
}

variable "deploy_domain" {}
variable "deploy_namespace" {}
variable "deploy_user" {}
variable "deploy_dir" {}
variable "deploy_private_key_path" {}
variable "host_public_keys" {}
variable "gitlab_api_token" {}
variable "gitlab_repo" {}
