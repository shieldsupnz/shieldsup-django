
provider "gitlab" {
  token = "${var.gitlab_api_token}"
  base_url = "https://${var.gitlab_domain}/api/v4/"
}

resource "gitlab_project_variable" "deploy_private_key" {
   project   = "${var.gitlab_repo}"
   key       = "${var.deploy_namespace}_DEPLOY_PRIVATE_KEY"
   value     = "${file("${var.deploy_private_key_path}")}"
   protected = true
   environment_scope = "*"
}

resource "gitlab_project_variable" "deploy_domain" {
   project   = "${var.gitlab_repo}"
   key       = "${var.deploy_namespace}_DEPLOY_DOMAIN"
   value     = "${var.deploy_domain}"
   protected = true
   environment_scope = "*"
}

resource "gitlab_project_variable" "deploy_user" {
   project   = "${var.gitlab_repo}"
   key       = "${var.deploy_namespace}_DEPLOY_USER"
   value     = "${var.deploy_user}"
   protected = true
   environment_scope = "*"
}

resource "gitlab_project_variable" "deploy_dir" {
   project   = "${var.gitlab_repo}"
   key       = "${var.deploy_namespace}_DEPLOY_DIR"
   value     = "${var.deploy_dir}"
   protected = true
   environment_scope = "*"
}

resource "gitlab_project_variable" "deploy_known_hosts" {
   project   = "${var.gitlab_repo}"
   key       = "${var.deploy_namespace}_DEPLOY_KNOWN_HOSTS"
   value     = "${var.host_public_keys}"
   protected = true
   environment_scope = "*"
}
