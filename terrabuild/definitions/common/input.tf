variable "app_name" {}
variable "domain_name" {}
variable "authorized_keys_path" {}
variable "deploy_user" {
  default = "deploy"
}
variable "deploy_dir" {
  default = "/usr/src/app"
}
variable "deploy_private_key_path" {}
variable "deploy_namespace" {}
variable "gitlab_repo" {}

variable "gitlab_api_token" {}
variable "digitalocean_api_token" {}

variable "public_key_path" {}
