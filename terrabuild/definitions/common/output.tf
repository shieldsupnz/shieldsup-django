output "base_domain" {
  value = "${var.domain_name}"
}

output "deploy_dir" {
  value = "${var.deploy_dir}"
}

output "deploy_namespace" {
  value = "${var.deploy_namespace}"
}

output "app_domain" {
  value = "${module.appserver.fqdn}"
}

output "server_ip" {
  value = "${module.appserver.ip}"
}

output "ssh" {
  value = "${module.appserver.ssh}"
}
