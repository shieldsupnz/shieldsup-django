# Python template
FROM python:3.7-alpine
# create root directory for project, set the working directory and move all files
RUN mkdir /shieldsup
WORKDIR /shieldsup

COPY ./shieldsup/Pipfile* /shieldsup/
RUN  apk add --no-cache postgresql-libs \
                   postgresql-client \
                   geos \
                   proj \
                   proj-util \
                   gdal \
                   gdal-tools \
                   binutils
RUN  apk add --no-cache --virtual .build-deps gcc \
                                          g++ \
                                          make \
                                          musl-dev \
                                          sqlite-dev \
                                          zlib-dev \
                                          proj-dev \
                                          gdal-dev \
                                          geos-dev \
                                          libxml2-dev \
                                          postgresql-dev && \
     wget https://gitlab.com/shieldsupnz/shieldsup-django-extra-packages/-/raw/master/libspatialite-4.3.0a.tar.gz  -O libspatialite.tar.gz && \
     tar xvf libspatialite.tar.gz && \
     cd libspatialite-4.3.0a/ && \
     CFLAGS="$CFLAGS -DACCEPT_USE_OF_DEPRECATED_PROJ_API_H=1" ./configure --enable-freexl=no --disable-static && \
     make && \
     make install && \
     cd .. && \
     rm -R libspatialite-4.3.0a libspatialite.tar.gz && \
     echo "START TIME: $(date -Iseconds)" && \
     python3 -m pip install pipenv --no-cache-dir && \
         cd /shieldsup && \
     pipenv install --deploy --system && \
     apk --purge del .build-deps && \
     echo "FINISH TIME: $(date -Iseconds)"

# Workaround for libraries not being found by geodjango
RUN ln -s $(find /usr/lib -iname libgdal.so.* -type f -exec basename {} \;) /usr/lib/libgdal.so && \
    ln -s $(find /usr/lib -iname libgeos_c.so.* -type f -exec basename {} \;) /usr/lib/libgeos_c.so && \
    ln -s $(find /usr/lib -iname libproj.so.* -type f -exec basename {} \;) /usr/lib/libproj.so

# Add source code at the end
ADD ./shieldsup /shieldsup

CMD ["/bin/bash", "start.sh"]
