from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from polymorphic.admin import (PolymorphicChildModelAdmin,
                               PolymorphicChildModelFilter,
                               PolymorphicParentModelAdmin)

from common.mixins import ExportCsv as ExportCsvMixin

from .models import (CNCMachine, LaserCutter, Machine, OtherMachine,
                     ThreeDimensionalPrinter)


class MachineCityFilter(admin.SimpleListFilter):
    """Excludes Customer cities from filter"""
    title = _('city')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'city'

    def lookups(self, request, model_admin):
        query = model_admin.model.objects.all().select_related('')
        results = query.values_list(
            'owner__city',
            flat=True).distinct().order_by('owner__city')
        targets = zip(results, results)
        return targets

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        return queryset.filter(owner__city=self.value())


class OwnerFilter(admin.SimpleListFilter):
    title = _('owner')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'owner'

    def lookups(self, request, model_admin):
        return (
            ('me', "Just mine"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'me':
            return queryset.filter(owner__user=request.user)

        return queryset


class MachineChildAdmin(PolymorphicChildModelAdmin, ExportCsvMixin):
    base_model = Machine
    autocomplete_fields = ("owner",)
    ordering = ('-modified_at',)
    readonly_fields = ('created_at', 'modified_at')
    list_filter = (OwnerFilter, "created_at", "modified_at", MachineCityFilter)
    list_select_related = ("owner",)
    search_fields = ('model', 'owner__name', 'owner__email', 'owner__city')
    actions = ['export_as_csv']
    base_fieldsets = (
        (_('Basic info'), {
            'fields': (
                'owner', 'model', 'maximum_output', 'bed_dimensions', 'state',
                'comments'
            )
        }),
        (_('Timestamps'), {
            'fields': ('created_at', 'modified_at')
        }),
    )

    def get_actions(self, request):
        actions = super().get_actions(request)
        if request.user.is_superuser or request.user.groups.filter(
                name='Admin').exists():
            return actions  # default permissions are ok
        # standard user - remove dangerous actions
        if 'delete_selected' in actions:
            del actions['delete_selected']
        if 'export_as_csv' in actions:
            del actions['export_as_csv']
        return actions

    def owner_name(self, obj):
        return mark_safe('{} <a href="{}">🔗</a>'.format(
            obj.owner.name,
            reverse("admin:contacts_contact_change", args=(obj.owner.pk,)),
        ))
    owner_name.short_description = "Owner Name"

    def owner_city(self, obj):
        return obj.owner.city

    def has_module_permission(self, request):
        is_admin = request.user.groups.filter(name="Admins").exists()
        return request.user.is_superuser or is_admin



@admin.register(ThreeDimensionalPrinter)
class ThreeDimensionalPrinterAdmin(MachineChildAdmin):
    base_model = ThreeDimensionalPrinter
    show_in_index = True
    list_display = (
        'id', 'model', 'printer_type', 'filament_diameter', 'maximum_output',
        'bed_dimensions', 'state', 'owner_name', 'owner_city', 'created_at',
        'modified_at'
    )
    list_display_links = list_display
    fieldsets = (
        (_('Basic info'), {
            'fields': (
                'owner', 'model', 'maximum_output', 'bed_dimensions',
                'filament_diameter', 'printer_type', 'state', 'comments'
            )
        }),
        (_('Timestamps'), {
            'fields': ('created_at', 'modified_at')
        }),
    )


@admin.register(LaserCutter)
class LaserCutterAdmin(MachineChildAdmin):
    base_model = LaserCutter
    show_in_index = True
    list_display = (
        'id', 'model', 'laser_wattage', 'maximum_output', 'bed_dimensions',
        'state', 'owner_name', 'owner_city', 'created_at', 'modified_at'
    )
    list_display_links = list_display
    fieldsets = (
        (_('Basic info'), {
            'fields': (
                'owner', 'model', 'maximum_output', 'bed_dimensions',
                'laser_wattage', 'state', 'comments'
            )
        }),
        (_('Timestamps'), {
            'fields': ('created_at', 'modified_at')
        }),
    )


@admin.register(CNCMachine)
class CNCMachineAdmin(MachineChildAdmin):
    base_model = CNCMachine
    show_in_index = True
    list_display = (
        'id', 'model', 'maximum_output', 'bed_dimensions', 'state',
        'owner_name', 'owner_city', 'created_at', 'modified_at'
    )
    list_display_links = list_display


@admin.register(OtherMachine)
class OtherMachineAdmin(MachineChildAdmin):
    base_model = OtherMachine


@admin.register(Machine)
class MachineAdmin(PolymorphicParentModelAdmin, ExportCsvMixin):
    base_model = Machine
    child_models = (
        LaserCutter,
        ThreeDimensionalPrinter,
        CNCMachine,
        OtherMachine)

    ordering = ('-modified_at',)
    readonly_fields = ('created_at', 'modified_at')
    list_filter = (
        PolymorphicChildModelFilter, OwnerFilter, "created_at", "modified_at",
        MachineCityFilter
    )
    list_select_related = ("owner",)
    autocomplete_fields = ("owner",)
    search_fields = ('model', 'owner__name', 'owner__email', 'owner__city')

    list_display = (
        'id', 'machine_type', 'model', 'maximum_output', 'bed_dimensions',
        'state', 'owner_name', 'owner_city', 'created_at', 'modified_at'
    )
    list_display_links = list_display
    actions = ['export_as_csv']

    def machine_type(self, obj):
        return obj.get_real_instance_class()._meta.verbose_name

    def owner_name(self, obj):
        return mark_safe('{} <a href="{}">🔗</a>'.format(
            obj.owner.name,
            reverse("admin:contacts_contact_change", args=(obj.owner.pk,))
        ))
    owner_name.short_description = "Owner Name"

    def owner_city(self, obj):
        return obj.owner.city

    def get_actions(self, request):
        actions = super().get_actions(request)
        is_admin = request.user.groups.filter(name='Admin').exists()
        if request.user.is_superuser or is_admin:
            return actions  # default permissions are ok
        # standard user - remove dangerous actions
        if 'delete_selected' in actions:
            del actions['delete_selected']
        if 'export_as_csv' in actions:
            del actions['export_as_csv']
        return actions
