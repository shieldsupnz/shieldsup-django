from collections import defaultdict

from django.db import models
from django.db.models import Min, Max, Count, Sum, Avg
from polymorphic.models import PolymorphicModel

# Create your models here.


class MachineCondition:
    GOOD = 'good'
    UNRELIABLE = 'unreliable'
    FAULTY = 'faulty'

    CHOICES = [
        (GOOD, "Good"),
        (UNRELIABLE, "Unreliable"),
        (FAULTY, "Faulty"),
    ]


class FilamentDiameter:
    FILAMENT_175 = '1.75mm'
    FILAMENT_300 = '3mm'
    FILAMENT_UNKNOWN = 'unknown'

    CHOICES = [
        (FILAMENT_175, "1.75mm"),
        (FILAMENT_300, "3mm"),
        (FILAMENT_UNKNOWN, "Unknown"),
    ]


class PrinterType:
    FDM = 'FDM'
    SLA = 'SLA'
    SLS = 'SLS'

    CHOICES = [
        (FDM, 'FDM (Filament Extruder)'),
        (SLA, 'SLA (Epoxy)'),
        (SLS, "SLS (Powder)"),
    ]


class Machine(PolymorphicModel):
    owner = models.ForeignKey('contacts.Contact', on_delete=models.CASCADE,
                              related_name='machines')
    model = models.CharField(max_length=255,
                             help_text='The make and model of the machine')
    maximum_output = models.IntegerField(
        "Maximum output per day", null=True, blank=True,
        help_text='The estimated maximum number of units the machine could '
                  'produce per day given infinite consumables (optional)')
    bed_dimensions = models.CharField(max_length=100, blank=True,
                                      help_text='Length x Width in mm (optional)')
    state = models.CharField(max_length=255, choices=MachineCondition.CHOICES,
                             default=MachineCondition.GOOD)
    comments = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return "{} ({}) [#{}]".format(self.model, self.owner.name, self.pk)
    __str__ = __repr__


class ThreeDimensionalPrinter(Machine):
    filament_diameter = models.CharField(max_length=255,
                                         choices=FilamentDiameter.CHOICES)
    printer_type = models.CharField(
        max_length=32, choices=PrinterType.CHOICES, default=PrinterType.FDM,
        help_text="You probably have a FDM-type printer (filament extruder) "
                  "not a SLA (epoxy laser) or a SLS (sinterer).")

    class Meta:
        verbose_name = "3d printer"


class LaserCutter(Machine):
    laser_wattage = models.IntegerField(blank=True, null=True,
                                        help_text="The power of the laser in Watts")


class CNCMachine(Machine):
    pass


class OtherMachine(Machine):
    pass


def get_machine_metrics():  # pylint: disable=too-many-locals
    """Compiles a dict of machine metrics

    Base paths:
        - /<region>/machine

    Subkeys:
        - /3dprinter
            - /count
            - /printer_type/<printer_type>
            - /filament_diameter/<filament_diameter>
            - /state/<condition>
            - /output_per_day
                - /total
                - /average
                - /minimum
                - /maximum
        - /lasercutter
            - /count
            - /state/<condition>
            - /output_per_day
                - /total
                - /average
                - /minimum
                - /maximum
        - /ALL
            - /count
    """
    root_key = '/ALL/machine'
    data = {}
    totals = defaultdict(int)
    states = list(map(lambda x: x[0], MachineCondition.CHOICES))
    # 3d printer metrics
    query = ThreeDimensionalPrinter.objects.all()
    count_3dprinter = len(query)
    filament_3dprinter = query.values('filament_diameter').annotate(count=Count('filament_diameter'))
    condition_3dprinter = query.values('state').annotate(count=Count('state'))
    query = query.exclude(maximum_output=None)
    capability_3dprinter = query.aggregate(count=Count('maximum_output'),
                                           total=Sum('maximum_output'),
                                           avg=Avg('maximum_output'),
                                           min=Min('maximum_output'),
                                           max=Max('maximum_output'))
    local_key = root_key + '/3dprinter'
    data[local_key + '/count'] = count_3dprinter
    totals['count'] += count_3dprinter
    for state in states:
        key = local_key + '/state/' + state
        data[key] = 0
    for filament in filament_3dprinter:
        key = local_key + '/filament_diameter/' + filament['filament_diameter']
        data[key] = filament['count']
    for condition in condition_3dprinter:
        key = local_key + '/state/' + condition['state']
        data[key] = condition['count']
    for field in ['count', 'total', 'avg', 'min', 'max']:
        key = local_key + '/output_per_day/' + field
        value = capability_3dprinter[field] or 0
        data[key] = value
    # laser cutter metrics
    query = LaserCutter.objects.all()
    lasercutter_count = len(query)
    lasercutter_condition = query.values('state').annotate(count=Count('state'))
    query = query.exclude(maximum_output=None)
    lasercutter_capability = query.aggregate(count=Count('maximum_output'),
                                             total=Sum('maximum_output'),
                                             avg=Avg('maximum_output'),
                                             min=Min('maximum_output'),
                                             max=Max('maximum_output'))
    local_key = root_key + '/lasercutter'
    data[local_key + '/count'] = lasercutter_count
    totals['count'] += lasercutter_count
    for state in states:
        key = local_key + '/state/' + state
        data[key] = 0
    for condition in lasercutter_condition:
        key = local_key + '/state/' + condition['state']
        data[key] = condition['count']
    for field in ['count', 'total', 'avg', 'min', 'max']:
        key = local_key + '/output_per_day/' + field
        data[key] = lasercutter_capability[field]
    data[root_key + '/ALL/count'] = totals['count']
    return data
