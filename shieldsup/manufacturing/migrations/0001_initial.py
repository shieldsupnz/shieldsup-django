# Generated by Django 3.0.4 on 2020-03-29 21:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contacts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Machine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kind', models.CharField(choices=[('3d printer', '3d printer'), ('laser cutter', 'Laser cutter')], max_length=255)),
                ('model', models.CharField(max_length=255)),
                ('state', models.CharField(choices=[('good', 'Good'), ('unreliable', 'Unreliable'), ('faulty', 'Faulty')], default='good', max_length=255)),
                ('comments', models.TextField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contacts.Manufacturer')),
            ],
        ),
    ]
