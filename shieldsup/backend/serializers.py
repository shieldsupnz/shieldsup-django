from rest_framework import serializers

# Disable W0223 because this serializer is not used to represent a single data
# model, rather an API request that is unpackaged into a 'customer' and an
# 'order'

# pylint: disable=W0223


class MedicalRequestSerializer(serializers.Serializer):

    is_medical = serializers.CharField(required=True, max_length=255)
    first_name = serializers.CharField(required=True, max_length=255)
    last_name = serializers.CharField(required=True, max_length=255)
    organisation = serializers.CharField(required=True, max_length=255)
    phone = serializers.CharField(required=True, max_length=255)
    email = serializers.EmailField(required=True, max_length=255)
    address = serializers.CharField(required=True)
    city = serializers.CharField(required=True, max_length=255)
    notes = serializers.CharField(required=True, allow_blank=True)
    needed = serializers.IntegerField(required=True)
    wanted = serializers.IntegerField(required=True)
