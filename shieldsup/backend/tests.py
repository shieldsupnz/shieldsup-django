import json
from copy import copy

from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from contacts.models import Customer
from orders.models import Order

User = get_user_model()

# Create your tests here.

class TestAPIV1MedicalRequest(APITestCase):
    def setUp(self):
        self.good_data = {
            "is_medical": "Checked",
            "first_name": "Test First Name",
            "last_name": "Test Last Name",
            "organisation": "Test Organization",
            "phone": "123456",
            "email": "029284a9-b1a6-43a3-8f70-c43e9af10f90@email.webhook.site",
            "address": "Test Address",
            "city": "Test City",
            "notes": "Test Notes",
            "needed": "5",
            "wanted": "10"
        }

        self.user = User.objects.create(email="admin@example.com",
                                        first_name="A", last_name="User",
                                        password=User.objects.make_random_password())
        self.token = Token.objects.create(user=self.user)
        self.url = '/api/v1/orders/webhook?api_token=' + self.token.key

    def test_post_bad(self):
        for key, _value in self.good_data.items():
            data = copy(self.good_data)

            if key == "notes":
                data['notes'] = ''
                response = self.client.post(self.url, {'data': json.dumps(data)})
                self.assertEqual(response.status_code, 201)
            else:
                del data[key]
                response = self.client.post(self.url, {'data': json.dumps(data)})
                self.assertEqual(response.status_code, 400)

    def test_post_good(self):
        data = copy(self.good_data)

        response = self.client.post(self.url, {'data': json.dumps(data)})

        self.assertEqual(response.status_code, 201)

        name = data['first_name'] + ' ' + data['last_name']

        customer = Customer.objects.get(name=name)
        self.assertEqual(customer.name, name)
        self.assertEqual(
            customer.is_medical_professional,
            data["is_medical"] == "Checked")
        self.assertEqual(customer.organization, data["organisation"])
        self.assertEqual(customer.phone, data["phone"])
        self.assertEqual(customer.address, data["address"])
        self.assertEqual(customer.city, data["city"])

        order = Order.objects.get(customer=customer)
        self.assertEqual(order.quantity_needed, int(data["needed"]))
        self.assertEqual(order.quantity_wanted, int(data["wanted"]))
        self.assertEqual(order.address, data["address"])
        self.assertEqual(order.city, data["city"])

        self.assertEqual(order.data_source, 'webhook')
        self.assertEqual(customer.data_source, 'webhook')
