from rest_framework import authentication, exceptions
from rest_framework.authtoken.models import Token


class QueryAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = request.GET.get('api_token')
        if not token:
            return None

        try:
            user = Token.objects.get(key=token).user
        except Token.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        return (user, None)
