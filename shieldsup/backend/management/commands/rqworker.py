import logging

from django_rq.management.commands import rqworker


class Command(rqworker.Command):
    def handle(self, *args, **options):
        # Verbosity doesnt work
        logging.basicConfig(level=logging.INFO)
        # Run as normal
        return super().handle(*args, **options)
