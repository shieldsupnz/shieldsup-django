
import logging

from django_rq import get_scheduler
from django_rq.management.commands import rqscheduler

from contacts.models import Contact
from orders.models import Order
from metrics.tasks import schedule_metrics_snapshot

class Command(rqscheduler.Command):
    def handle(self, *args, **options):
        # Verbosity doesnt work
        logging.basicConfig(level=logging.INFO)
        scheduler = get_scheduler(options.get('queue'))
        # Delete any existing jobs in the scheduler when the app starts up
        for job in scheduler.get_jobs():
            job.delete()
        # Auto-add any scheduling jobs
        Contact.enqueue_geocoding_tasks()
        Order.enqueue_geocoding_tasks()
        schedule_metrics_snapshot("0 0 * * *")
        
        # Run as normal
        return super().handle(*args, **options)
