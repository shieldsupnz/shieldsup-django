
import logging

from django.core.management.base import BaseCommand

from backend.tasks import refresh_requests_airtable


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Verbosity doesnt work
        logging.basicConfig(level=logging.INFO)
        refresh_requests_airtable()
