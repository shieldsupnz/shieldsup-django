# Two tasks
# pylint: skip-file
import json
import logging
from collections import defaultdict
from datetime import MINYEAR, datetime

import pytz
from airtable import Airtable
from django.conf import settings
from django.db import transaction
from django.utils import dateparse, timezone
from django_rq import get_scheduler, job
from rq import get_current_job

from contacts.models import Customer, Manufacturer, Supplier
from orders.models import Order, RejectedOrder

MINIMUM_DATETIME = datetime(MINYEAR, 1, 1, tzinfo=pytz.utc)
logger = logging.getLogger('backend_tasks')


@job
def refresh_requests_airtable_job():
    pass  # disabled due to airtable being deprecated
    # refresh_requests_airtable()


def refresh_comments_airtable():
    api_key = settings.AIRTABLE_API_KEY
    if api_key is None:
        raise Exception('The environment variable AIRTABLE_API_KEY is not set')
    refresh_base_id = settings.AIRTABLE_REFRESH_BASE_ID
    refresh_table_name = settings.AIRTABLE_REFRESH_TABLE_NAME

    stats = defaultdict(int)

    requests_table = Airtable(
        refresh_base_id,
        refresh_table_name,
        api_key=api_key)
    for page in requests_table.get_iter():
        for record in page:
            fields = record['fields']
            # Cleanse the data
            notes = record['fields'].get('Notes', '')
            airtable_ref = record['id']

            if airtable_ref != '':
                try:
                    with transaction.atomic():
                        order = Order.objects.get(airtable_ref=airtable_ref)
                        notes_bits = fields.get('Notes', '').split("\n")
                        notes = "\n".join([b.strip()
                                           for b in notes_bits]).strip()
                        order.comments = notes
                        order.save()
                        stats['successful'] += 1
                except Order.DoesNotExist:
                    logger.error(
                        'Order does not exist for airtable_ref "{}"'.format(airtable_ref))
                    stats['failed'] += 1
    logger.info(
        'Comments load from airtable (base id="{}", table name="{}"):'.format(
            refresh_base_id,
            refresh_table_name))
    logger.info(' {} successful, {} failed, {} total'.format(stats['successful'],
                                                             stats['failed'],
                                                             sum(stats.values())))


def refresh_requests_airtable():
    api_key = settings.AIRTABLE_API_KEY
    if api_key is None:
        raise Exception('The environment variable AIRTABLE_API_KEY is not set')
    refresh_base_id = settings.AIRTABLE_REFRESH_BASE_ID
    refresh_table_name = settings.AIRTABLE_REFRESH_TABLE_NAME

    stats = defaultdict(int)

    requests_table = Airtable(
        refresh_base_id,
        refresh_table_name,
        api_key=api_key)
    for page in requests_table.get_iter():
        for record in page:
            fields = record['fields']
            # Cleanse the data
            name = fields.get('Name', '').strip()
            organization = fields.get('Organisation', '').strip()
            email = fields.get('Email Address', '').strip()
            phone = fields.get('Contact Phone', '').strip()
            address_bits = fields.get('Address', '').split("\n")
            address = "\n".join([b.strip() for b in address_bits]).strip()
            city = fields.get('City', '').strip()
            airtable_ref = record['id']
            try:
                needed = int(
                    fields.get(
                        'How Many Needed (minimum to keep you safe)',
                        '0'))
            except BaseException:
                needed = 0
            try:
                wanted = int(
                    fields.get(
                        'How Many Wanted if We Have Spares?',
                        '0'))
            except BaseException:
                wanted = 0
            is_medical_professional = fields.get(
                'Are you a medical professional?', '').lower().strip() in [
                'yes', 'true', 'checked']
            created_at = dateparse.parse_datetime(fields['Created Time'])
            if 'Last Modified Time' in fields:
                modified_at = dateparse.parse_datetime(
                    fields['Last Modified Time'])
            else:
                modified_at = created_at  # change to minimum possible datetime

            with transaction.atomic():
                if (name == '' or email == '' or city == '' or address == '') or (
                        needed == 0 and wanted == 0):
                    try:
                        rejected_order = RejectedOrder.objects.get(
                            airtable_ref=record['id'])
                        if rejected_order.modified_at < modified_at:
                            rejected_order.name = name
                            rejected_order.email = email
                            rejected_order.phone = phone
                            rejected_order.address = address
                            rejected_order.city = city
                            rejected_order.needed = needed
                            rejected_order.wanted = wanted
                            rejected_order.data = json.dumps(record)
                            rejected_order.modified_at = modified_at
                            rejected_order.data_source = "airtable"
                            rejected_order.save()
                            RejectedOrder.objects.filter(
                                pk=rejected_order.pk).update(
                                modified_at=modified_at)
                        else:
                            stats['skipped'] += 1
                            continue
                    except RejectedOrder.DoesNotExist:
                        logger.error(
                            'Record "{}" rejected due to invalid data'.format(airtable_ref))
                        rejected_order = RejectedOrder()
                        rejected_order.airtable_ref = airtable_ref
                        rejected_order.name = name
                        rejected_order.email = email
                        rejected_order.phone = phone
                        rejected_order.address = address
                        rejected_order.city = city
                        rejected_order.needed = needed
                        rejected_order.wanted = wanted
                        rejected_order.data = json.dumps(record)
                        rejected_order.modified_at = modified_at
                        rejected_order.data_source = "airtable"
                        rejected_order.save()
                        RejectedOrder.objects.filter(
                            pk=rejected_order.pk).update(
                            modified_at=modified_at)
                    stats['failed'] += 1
                    continue
                else:
                    # row is no longer an issue - delete it
                    RejectedOrder.objects.filter(
                        airtable_ref=airtable_ref).delete()

                try:
                    customer = Customer.objects.get(email=email)
                except Customer.DoesNotExist:
                    customer = Customer.objects.create(
                        name=name,
                        organization=organization,
                        email=email,
                        phone=phone,
                        address=address,
                        city=city,
                        airtable_ref=airtable_ref,
                        data_source="airtable",
                        is_medical_professional=is_medical_professional,
                    )
                    Customer.objects.filter(
                        pk=customer.pk).update(
                        created_at=created_at,
                        modified_at=modified_at)

                try:
                    order = Order.objects.get(airtable_ref=airtable_ref)
                    if order.modified_at < modified_at:
                        order.address = address
                        order.city = city
                        order.quantity_needed = needed
                        order.quantity_wanted = wanted
                        order.data_source = "airtable"
                        order.save()
                        Order.objects.filter(
                            pk=order.pk).update(
                            modified_at=modified_at)
                    else:
                        stats['skipped'] += 1
                        continue
                except Order.DoesNotExist:
                    order = Order.objects.create(
                        customer=customer,
                        quantity_needed=needed,
                        quantity_wanted=wanted,
                        airtable_ref=airtable_ref,
                        address=address,
                        city=city,
                        data_source="airtable",
                    )
                    Order.objects.filter(
                        pk=order.pk).update(
                        created_at=created_at,
                        modified_at=modified_at)

            stats['successful'] += 1
    logger.info('Data load from airtable (base id="{}", table name="{}"):'.format(
        refresh_base_id,
        refresh_table_name))
    logger.info(' {} successful, {} failed, {} skipped, {} total'.format(stats['successful'],
                                                                         stats['failed'],
                                                                         stats['skipped'],
                                                                         sum(stats.values())))


def setup_scheduler():
    scheduler = get_scheduler('default')
    # Have 'mytask' run every 5
    scheduler.schedule(datetime.utcnow(), refresh_requests_airtable_job,
                       interval=300, result_ttl=1200)
