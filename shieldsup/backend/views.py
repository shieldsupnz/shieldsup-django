import json
import logging

from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.serializers import MedicalRequestSerializer
from contacts.models import Customer
from orders.models import Order

from .authentication import QueryAuthentication

logger = logging.getLogger("backend")


class MedicalRequest(APIView):
    authentication_classes = [QueryAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):  # pylint: disable=unused-argument,redefined-builtin
        data = json.loads(request.POST['data'])
        serializer = MedicalRequestSerializer(data=data)
        if serializer.is_valid():
            try:
                customer = Customer.objects.get(email=serializer.validated_data['email'])
            except Customer.DoesNotExist:
                customer = Customer.objects.create(
                    name=(serializer.validated_data['first_name'] + " " +
                          serializer.validated_data['last_name']),
                    organization=serializer.validated_data['organisation'],
                    email=serializer.validated_data['email'],
                    phone=serializer.validated_data['phone'],
                    address=serializer.validated_data['address'],
                    city=serializer.validated_data['city'],
                    is_medical_professional=serializer.validated_data['is_medical'] == "Checked",
                    data_source="webhook",
                )

            Order.objects.create(
                customer=customer,
                quantity_needed=serializer.validated_data['needed'],
                quantity_wanted=serializer.validated_data['wanted'],
                address=serializer.validated_data['address'],
                city=serializer.validated_data['city'],
                comments=serializer.validated_data['notes'],
                data_source="webhook",
            )
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        logger.error("Webhook for orders serializer failed: %s", serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
