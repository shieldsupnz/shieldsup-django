from django.db import models  # pylint: disable=unused-import
from django.utils import timezone  # pylint: disable=unused-import

# Create your models here.


# class MetricKind(models.Model):
#     name = models.CharField(max_length=255)
#     label = models.CharField(max_length=255, blank=True)
#     description = models.CharField(max_length=255, blank=True)


# class MetricRecord(PolymorphicModel):
#     kind = models.ForeignKey('MetricKind', on_delete=models.CASCADE)
#     timestamp = models.DatetimeField(default=timezone.now)


# class MetricInteger(Statistic):
#     value = models.IntegerField()


# class MetricFloat(Statistic):
#     value = models.FloatField()
