from datetime import datetime

from django.views.decorators.cache import cache_page
from django.utils import timezone
from django.shortcuts import redirect
from django.core.files.storage import default_storage


from rest_framework.response import Response
from rest_framework.decorators import (
    api_view, renderer_classes, permission_classes, authentication_classes
)
from rest_framework.exceptions import ParseError
from rest_framework.renderers import JSONRenderer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication

from .tasks import (
    capture_project_metrics, metrics_as_hierarchy, project_metrics_labels,
    capture_order_metrics_for_user,
    metrics_snapshot_key
)


# cache for 60 seconds
@cache_page(60)
@api_view(['GET'])
@renderer_classes([JSONRenderer])
def metrics_current_view(request):
    flat_metrics = capture_project_metrics()
    output = {
        'timestamp': timezone.now().isoformat(),
        'data': metrics_as_hierarchy(flat_metrics),
        'labels': project_metrics_labels(),
    }
    return Response(output)


@cache_page(60)
@api_view(['GET'])
@authentication_classes([SessionAuthentication])
@permission_classes([IsAuthenticated])
@renderer_classes([JSONRenderer])
def metrics_for_user_current_view(request):
    metrics = capture_order_metrics_for_user(request.user)
    output = {
        'timestamp': timezone.now().isoformat(),
        'user': str(request.user),
        'data': dict(metrics),
    }
    return Response(output)


@api_view(['GET'])
def metrics_historical_view(request, isodate):
    try:
        snapshot_timestamp = datetime.strptime(isodate, "%Y-%m-%d")
    except ValueError:
        raise ParseError('Invalid date not in format "YYYY-MM-DD"')
    snapshot_url = default_storage.url(metrics_snapshot_key(snapshot_timestamp))
    return redirect(snapshot_url)
