from collections import defaultdict
from datetime import datetime
import logging
import json
import django_rq


from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.db import transaction
from django.utils import timezone


from orders.models import get_order_metrics, get_order_metrics_for_manufacturer
from products.models import get_product_metrics, get_product_metrics_labels
from contacts.models import get_contact_metrics, Manufacturer
from manufacturing.models import get_machine_metrics
from users.models import get_user_metrics

logger = logging.getLogger('metrics')


def metrics_snapshot_key(isodatetime):
    return 'metrics/{}.json'.format(isodatetime.strftime("%Y-%m-%d"))


def snapshot_metrics():
    flat_metrics = capture_project_metrics()
    output = {
        'timestamp': timezone.now().isoformat(),
        'data': metrics_as_hierarchy(flat_metrics),
        'labels': project_metrics_labels(),
    }

    now = datetime.now()
    logger.info("Prepared metrics snapshot at %s", now.isoformat())
    data_bytes = json.dumps(output).encode()
    default_storage.save(metrics_snapshot_key(now), ContentFile(data_bytes))


def schedule_metrics_snapshot(cron_string):
    logger.info("Schedling metrics snapshot using cron schedule: %s", cron_string)
    scheduler = django_rq.get_scheduler('default')
    scheduler.cron(
        cron_string=cron_string,
        func=snapshot_metrics,
        queue_name='default'
    )


def capture_project_metrics():
    """Grab a snapshot of current statistics from the database

    Returns a dict in format:
       {
           'path/to/key': value
       }
    """
    data = {}
    with transaction.atomic():
        data.update(get_order_metrics())
        data.update(get_machine_metrics())
        data.update(get_contact_metrics())
        data.update(get_product_metrics())
        data.update(get_user_metrics())
    return data


def recursive_defaultdict():
    return defaultdict(recursive_defaultdict)


def metrics_as_hierarchy(flat_metrics):
    data = recursive_defaultdict()
    for path, value in flat_metrics.items():
        bits = path.strip('/').split('/')
        if len(bits) == 0:
            logger.warning('path "%s" found with zero parts', path)
            continue
        node = data
        for key in bits[:-1]:
            node = node[key]
        node[bits[-1]] = value
    return data


def project_metrics_labels():
    labels = {}
    labels.update(get_product_metrics_labels())
    return labels


def capture_order_metrics_for_user(user):
    if not user.is_authenticated:
        return {}
    manufacturer = Manufacturer.objects.filter(user=user).first()
    if not manufacturer:
        return {}
    return get_order_metrics_for_manufacturer(manufacturer)
