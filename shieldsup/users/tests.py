from copy import copy

from django.contrib.auth.models import Group
from django.db.utils import IntegrityError
from django.test import TestCase
from rest_framework.test import APITestCase

from contacts.models import Manufacturer

from .models import User


class UserModelTest(TestCase):
    """User model tests"""

    def test_user_create(self):
        # SUCCESS
        User.objects.create(email="user@example.com",
                            first_name="Test",
                            last_name="User",
                            password="abcdef1234")
        User.objects.create(email="admin@example.com",
                            first_name="Admin",
                            last_name="User",
                            is_superuser=True,
                            password="abcdef1234")

    def test_user_duplicate_email(self):
        User.objects.create(email="user@example.com",
                            first_name="Test",
                            last_name="User",
                            password="abcdef1234")
        # FAIL - duplicate email
        try:
            User.objects.create(email="user@example.com",
                                first_name="Test",
                                last_name="User",
                                password="abcdef1234")
            self.fail(
                'Creating a user with the same email as an existing user should not succeed')
        except IntegrityError:
            pass
        # SUCCESS


class TestAPIV1UserCreate(APITestCase):
    def setUp(self):
        self.makers_group = Group.objects.create(name="Makers")
        self.system_group, _ = Group.objects.get_or_create(name="System")
        self.good_data = {
            "first_name": "John ",
            "last_name": "Smith ",
            "email": "john@example.com ",
            "phone": "01231023 ",
            "street_address": "  12 Smith St",
            "suburb": "Townsville",
            "city": "  Auckland",
            "postcode": "1231",
            "comments": "   ",
            "able_to_deliver_locally": True
        }
        self.url = '/api/v1/users/webhook'

        self.webhook_user = User.objects.create(
            first_name="Webhook", last_name="User", email="webhook@localhost")
        self.webhook_user.groups.add(self.system_group)

    def test_no_auth_fails(self):
        response = self.client.post(self.url, self.good_data, format="json")
        self.assertEqual(response.status_code, 401)

    def test_post_good(self):
        data = copy(self.good_data)
        self.client.force_authenticate(user=self.webhook_user)

        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, 201)

        # check models
        name = data['first_name'].strip() + ' ' + data['last_name'].strip()
        manufacturer = Manufacturer.objects.get(name=name)
        self.assertEqual(manufacturer.name, name)
        address = "\n".join([
            data['street_address'].strip(),
            data['suburb'].strip(),
            data['city'].strip() + " " + data['postcode'].strip()
        ])
        self.assertEqual(manufacturer.address, address)
        self.assertEqual(manufacturer.email, data["email"].strip())
        self.assertEqual(manufacturer.phone, data["phone"].strip())
        self.assertEqual(manufacturer.city, data["city"].strip())
        self.assertEqual(
            manufacturer.can_courier_unaided,
            data["able_to_deliver_locally"])
        self.assertEqual(manufacturer.comments, data["comments"].strip())
        self.assertTrue(
            manufacturer.data_source.startswith("webhook_usercreate"))

        user = User.objects.get(email=data["email"].strip())
        self.assertEqual(user.first_name, data['first_name'].strip())
        self.assertEqual(user.last_name, data['last_name'].strip())
        self.assertEqual(user.is_staff, True)
        self.assertIn(self.makers_group, user.groups.all())

        self.assertEqual(manufacturer.user, user)

        # check response
        self.assertEqual(response.data['created'], True)
        login = response.data['login']
        self.assertEqual(login['email'], user.email)
        self.assertIn("password", login)

        # test duplicate fails
        response = self.client.post(self.url, data, format="json")
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['created'], False)
        self.assertEqual(response.data['reason'], "user exists")

    def test_post_bad(self):
        self.client.force_authenticate(user=self.webhook_user)
        for key, _val in self.good_data.items():
            data = copy(self.good_data)

            if key == "comments":
                continue

            del data[key]
            response = self.client.post(self.url, data, format="json")
            self.assertEqual(response.status_code, 400)
