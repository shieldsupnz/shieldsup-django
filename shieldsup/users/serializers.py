
from rest_framework import serializers
from users.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')


# Disable W0223 because this serializer is not used to represent a single data
# model, rather an API request that is unpackaged into a 'user' and a
# 'contact'

# pylint: disable=W0223
class UserCreateSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, max_length=255)
    first_name = serializers.CharField(required=True, max_length=255)
    last_name = serializers.CharField(required=True, max_length=255)
    phone = serializers.CharField(required=True, max_length=255, allow_blank=True)
    street_address = serializers.CharField(required=True, max_length=255)
    suburb = serializers.CharField(required=True, max_length=255)
    city = serializers.CharField(required=True, max_length=255)
    postcode = serializers.CharField(required=True, max_length=255)
    comments = serializers.CharField(required=True, allow_blank=True)
    able_to_deliver_locally = serializers.BooleanField(required=True)
