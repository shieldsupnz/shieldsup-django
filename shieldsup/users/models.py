from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from .managers import UserManager

# Create your models here.


class InsensitiveEmailField(models.EmailField):
    def get_prep_value(self, value):
        """
        Lower-cases the value returned by super, while still allowing nullable email fields.
        """
        prep_value = super(InsensitiveEmailField, self).get_prep_value(value)
        if prep_value is not None:
            return prep_value.lower()
        return prep_value


class User(AbstractUser):
    """User model for shieldsup"""
    username = None  # remove the username field
    email = InsensitiveEmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def __repr__(self):
        return "{} {} ({}) [#{}]".format(self.first_name, self.last_name,
                                         self.email, self.pk)
    __str__ = __repr__

    objects = UserManager()


def get_user_metrics():
    """Compiles a dict of user metrics

    Base paths:
        - /<region>/user

    Subkeys:
        - /ALL/count
        - /<group>/count
    """
    root_key = '/ALL/user'
    data = {}
    query = User.objects.filter(is_staff=True, is_active=True)
    data[root_key + '/maker/count'] = len(query.filter(groups__name='Makers'))
    subquery = query.filter(groups__name='Hub Coordinators')
    data[root_key + '/hub_coordinator/count'] = len(subquery)
    subquery = query.filter(Q(groups__name='Admins') | Q(is_superuser=True))
    data[root_key + '/admin/count'] = len(subquery)
    data[root_key + '/ALL/count'] = sum(data.values())
    return data
