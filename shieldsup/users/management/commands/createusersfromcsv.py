import logging

from django.core.management.base import BaseCommand

from users.tasks import bulk_create_users_from_csv


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('input_path', nargs=1)
        parser.add_argument('output_path', nargs=1)

    def handle(self, *args, **options):
        logging.basicConfig(level=logging.INFO)
        input_path = options['input_path'][0]
        output_path = options['output_path'][0]
        bulk_create_users_from_csv(input_path, output_path)
        # Verbosity doesnt work
        logging.basicConfig(level=logging.INFO)
