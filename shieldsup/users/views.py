import logging
from django.contrib.auth.models import Group
from django.db import transaction
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token


from contacts.models import Manufacturer

from .models import User
from .serializers import UserCreateSerializer

# Create your views here.

logger = logging.getLogger("users")


class UserCreate(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None): # pylint: disable=unused-argument,redefined-builtin,too-many-locals
        data = request.data
        serializer = UserCreateSerializer(data=data)
        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        if not request.user.groups.filter(name="System").exists():
            return Response({}, status=status.HTTP_403_FORBIDDEN)

        makers_group = Group.objects.get(name="Makers")

        with transaction.atomic():
            # create or get User
            email = serializer.validated_data['email'].strip()
            first_name = serializer.validated_data['first_name'].strip()
            last_name = serializer.validated_data['last_name'].strip()
            phone = serializer.validated_data['phone'].strip()
            street_address = serializer.validated_data['street_address'].strip()
            suburb = serializer.validated_data['suburb'].strip()
            city = serializer.validated_data['city'].strip()
            postcode = serializer.validated_data['postcode'].strip()
            comments = serializer.validated_data['comments'].strip()
            able_to_deliver_locally = serializer.validated_data['able_to_deliver_locally']

            full_name = first_name + " " + last_name

            address = "\n".join([
                street_address,
                suburb,
                city + " " + postcode,
            ])

            try:
                user = User.objects.get(email=email)
                return Response({
                    'created': False,
                    'reason': 'user exists',
                    'message': 'User for email="{}" already exists'.format(email)
                }, status=status.HTTP_200_OK)
            except User.DoesNotExist:

                logger.debug('User(%s, %s, %s, %s)',
                             first_name,
                             last_name,
                             email,
                             '<password>')

                logger.debug('Manufacturer(%s, %s, %s, %s, %s, %s, %s,)',
                             full_name,
                             phone,
                             email,
                             address,
                             city,
                             comments,
                             able_to_deliver_locally)

                user = User.objects.create(
                    is_staff=True,
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                )
                password = User.objects.make_random_password()
                user.set_password(password)
                user.save()

                response_data = {
                    "login": {
                        "email": email,
                        "password": password,
                    },
                    "created": True
                }

                # Add to Makers group by default
                makers_group.user_set.add(user)

                # dodgy bit - could be multiple results
                manufacturers = Manufacturer.objects.filter(
                    email__iexact=user.email)
                if len(manufacturers) == 1:
                    manufacturer = manufacturers[0]
                    manufacturer.user = user
                    manufacturer.save()
                elif len(manufacturers) == 0:
                    manufacturer = Manufacturer.objects.create(
                        name=full_name,
                        phone=phone,
                        email=email,
                        address=address,
                        city=city,
                        comments=comments,
                        can_courier_unaided=able_to_deliver_locally,
                        data_source="webhook_usercreate",
                        user=user,
                    )
                else:
                    logger.warning(
                        'Cannot assign user "%s" to multiple manufacturers (ids found %s)',
                        email, [c.id for c in manufacturers])
            return Response(response_data, status=status.HTTP_201_CREATED)


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        logger.warning(request)
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        manufacturer = Manufacturer.objects.filter(user=user).first()
        token, _ = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email,
            'first_name':user.first_name,
            'last_name':user.last_name,
            'manufacturer_id': manufacturer.pk
        })
