from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.models import LogEntry

from .models import User


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    list_display = (
        'action_time', 'user', 'content_type', 'object_id', 'object_repr',
        'action_flag', 'change_message'
    )
    select_related = ('user', 'content_type')
    search_fields = (
        'user__first_name', 'user__last_name', 'user__email',
        'content_type__app_label', 'content_type__model', 'object_repr',
        'change_message',
    )
    list_filter = ('action_flag', 'content_type', 'action_time')
    readonly_fields = list_display

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {
            'fields': ('email', 'password')
        }),
        (_('Personal info'), {
            'fields': ('first_name', 'last_name')
        }),
        (_('Permissions'), {
            'fields': (
                'is_active', 'is_staff', 'is_superuser', 'groups',
                'user_permissions'
            )
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined')
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    list_display_links = list_display
    search_fields = ('email', 'first_name', 'last_name')
    readonly_fields = ['last_login', 'date_joined']
    ordering = ('email',)
