import csv
import logging
from collections import defaultdict

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import transaction
from django.utils import timezone

from contacts.models import Contact, Manufacturer

User = get_user_model()


logger = logging.getLogger('users')


# pylint: disable=too-many-locals, too-many-branches, too-many-statements
def bulk_create_users_from_csv(input_path, output_path):
    infile = open(input_path, newline='')
    timestamp = timezone.now().isoformat()
    reader = csv.DictReader(infile)
    outfile = open(output_path, 'a', newline='')
    user_details = []

    user_stats = defaultdict(int)
    group_stats = defaultdict(int)
    contact_stats = defaultdict(int)

    with transaction.atomic():
        hub_group = Group.objects.get(name='Hub Coordinators')
        makers_group = Group.objects.get(name='Makers')

        for row in reader:

            # create or get User
            try:
                user = User.objects.get(email=row['Email'].strip())
                logger.warning(
                    'User %s %s (%s) already exists',
                    user.first_name, user.last_name, user.email)
                user_stats['skipped'] += 1
            except User.DoesNotExist:
                user = User.objects.create(
                    is_staff=True,
                    first_name=row["First name"].strip(),
                    last_name=row["Last name"].strip(),
                    email=row["Email"].strip(),
                )
                password = User.objects.make_random_password()
                user.set_password(password)
                user.save()

                details = [
                    "{} {}".format(user.first_name, user.last_name),
                    "Hi {}, you requested a login for the webapp. Here are the details:".format(
                        user.first_name),
                    "https://app.shieldsup.org.nz/",
                    user.email,
                    password
                ]
                user_details += ["\n".join(details) + "\n\n"]

                user_stats['added'] += 1

            # add user to group if not already added
            role = row["Role"]
            if role == "Maker":
                if makers_group.user_set.filter(pk=user.pk).exists():
                    logger.warning('User %s %s (%s) is already a member of group Makers',
                                   user.first_name, user.last_name, user.email)
                    group_stats['skipped'] += 1
                else:
                    makers_group.user_set.add(user)
                    group_stats['added'] += 1

            elif role == "Hub Coordinator":
                if hub_group.user_set.filter(pk=user.pk).exists():
                    logger.warning('User %s %s (%s) is already a member of group Hub Coordinators',
                                   user.first_name, user.last_name, user.email)
                    group_stats['skipped'] += 1
                else:
                    hub_group.user_set.add(user)
                    group_stats['added'] += 1
            else:
                logger.warning('Invalid group %s given for user %s %s (%s)',
                               role, user.first_name, user.last_name, user.email)
                group_stats['skipped'] += 1

            # check contact before and then add if doesn't exist
            found = False
            contacts = Manufacturer.objects.filter(email__iexact=user.email)
            if len(contacts) > 0:
                found = True
                matches = ", ".join(
                    ["#{} {}".format(contact.pk, contact.name) for contact in contacts])
                logger.warning('Manufacturers %s with same email as user %s %s (%s) already exist',
                               matches, user.first_name, user.last_name, user.email)

            try:
                contact = Contact.objects.get(user=user)
                found = True
                logger.warning('User %s %s (%s) is already attached to Contact #%s %s',
                               user.first_name, user.last_name, user.email, contact.pk, contact.name)
            except Contact.DoesNotExist:
                pass

            if found is False:
                address = "\n".join([
                    row["Street Address"].strip(),
                    row["Suburb"].strip(),
                    row["City"].strip() + " " + row["Postcode"].strip(),
                ])

                Manufacturer.objects.create(
                    name=" ".join([row["First name"].strip(),
                                   row["Last name"].strip()]),
                    phone=row["Phone"].strip(),
                    email=row["Email"].strip(),
                    address=address,
                    city=row["City"].strip(),
                    comments=row["Other comments or notes"].strip(),
                    can_courier_unaided=row["Are you able to perform local deliveries?"] == "Yes",
                    data_source="bulk_create {}".format(timestamp),
                    user=user,
                )
                contact_stats['added'] += 1
            else:
                contact_stats['skipped'] += 1

        outfile.write('\n-------- RUN {} --------\n\n'.format(timestamp))
        for details in user_details:
            outfile.write(details)

        outfile.close()

    print("Successfully created {} users, skipped {}".format(
        user_stats['added'], user_stats['skipped']))
    print("Successfully added {} users to groups, skipped {}".format(
        group_stats['added'], group_stats['skipped']))
    print("Successfully added {} users to manufacturers, skipped {} users".format(
        contact_stats['added'], contact_stats['skipped']))
