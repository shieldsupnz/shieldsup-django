from django.contrib import admin
from django.utils.translation import gettext_lazy as _

class RegionFilter(admin.SimpleListFilter):
    title = _('region name')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'region_name'

    def lookups(self, request, model_admin):
        query = model_admin.model.objects.all().select_related('')
        query = query.exclude(region=None)
        query = query.values_list('region__name', flat=True).distinct()
        results = query.order_by('region__name')
        targets = zip(results, results)
        return targets

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        return queryset.filter(region__name=self.value())
