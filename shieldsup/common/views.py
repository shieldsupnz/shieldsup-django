from django.template.loader import get_template
from django.http import HttpResponse


def static_view(template_name):
    """Simple view that serves html file"""
    def inner(request):
        template = get_template(template_name)
        return HttpResponse(template.template.source)
    return inner
