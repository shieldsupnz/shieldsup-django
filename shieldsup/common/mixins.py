import csv

from django.http import HttpResponse


class AdminActionsMixin:
    def get_actions(self, request):
        actions = super().get_actions(request)
        is_admin = request.user.groups.filter(name='Admin').exists()
        if request.user.is_superuser or is_admin:
            return actions  # default permissions are ok
        # standard user - remove dangerous actions
        if 'delete_selected' in actions:
            del actions['delete_selected']
        if 'export_as_csv' in actions:
            del actions['export_as_csv']
        return actions


class ExportCsv:
    def export_as_csv(self, request, queryset):
        meta = self.model._meta

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % meta

        field_names = self.model_csv_attributes()
        writer = csv.writer(response)
        writer.writerow(field_names)
        for obj in queryset:
            writer.writerow(self.model_to_csv(obj, field_names))

        return response

    export_as_csv.short_description = "Export Selected"

    # Override this method to determine what the headers and
    # field names to resolve will be for a specific view.
    def model_csv_attributes(self):
        return [field.name for field in self.model._meta.fields]

    # Override this method to determine how to convert a given
    # object into a list representing a CSV row.
    def model_to_csv(self, model, field_names):
        return [getattr(model, field) for field in field_names]
