from django.contrib.auth.models import Group

from rest_framework.test import APITestCase

from users.models import User
from contacts.models import Manufacturer, Customer
from orders.models import Order
from products.models import Product


class ShieldsUPAPITestCase(APITestCase):  # pylint: disable=too-many-instance-attributes
    def setUp(self):
        self.user = User.objects.create(email="user@example.com",
                                        first_name="Test",
                                        last_name="User")
        self.manufacturer = Manufacturer.objects.create(
            user=self.user,
            name="Test User",
            email="test@example.com",
            address="1234 Smith St",
            city="Auckland",
            can_courier_unaided=True,
        )

        hub, _ = Group.objects.get_or_create(name="Hub Coordinators")
        self.hub_user = User.objects.create(email="hub@example.com",
                                            first_name="Hub",
                                            last_name="User")
        self.hub_user.groups.add(hub)

        self.other_user = User.objects.create(email="other@example.com",
                                              first_name="Other",
                                              last_name="User")

        self.other_manufacturer = Manufacturer.objects.create(
            user=self.other_user,
            name="Other User",
            email="other@example.com",
            address="1000 Smith St",
            city="Auckland",
            can_courier_unaided=True,
        )

        self.customer = Customer.objects.create(
            name="Customer Person",
            email="customer@example.com",
            address="1234 Smith St",
            city="Auckland",
        )

        self.other_customer = Customer.objects.create(
            name="Customer Other",
            email="other_customer@example.com",
            address="1234 Smith St",
            city="Auckland",
        )

        self.order = Order.objects.create(
            customer=self.customer,
            address=self.customer.address,
            city=self.customer.city,
            quantity_needed=40,
            quantity_wanted=100,
        )

        self.other_order = Order.objects.create(
            customer=self.other_customer,
            address=self.customer.address,
            city=self.customer.city,
            quantity_needed=40,
            quantity_wanted=100,
            assigned_to=self.other_manufacturer,
        )

        self.product = Product.objects.create(
            name="Test Product",
            kind="consumable",
        )

        self.other_product = Product.objects.create(
            name="Other Product",
            kind="consumable",
        )
