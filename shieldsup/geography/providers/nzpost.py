from datetime import timedelta
from alf.client import Client
from ratelimit import limits
from requests import RequestException
from redis import StrictRedis
from django.conf import settings

ONE_SECOND = 1


class NZPostError(Exception):
    pass


class AddressChecker:
    redis = StrictRedis(host=settings.REDIS_HOST, port=6379, db=0)
    alf = Client(
        token_endpoint='https://oauth.nzpost.co.nz/as/token.oauth2',
        client_id=settings.NZPOST_OAUTH_CLIENT_ID,
        client_secret=settings.NZPOST_OAUTH_CLIENT_SECRET,
    )
    host = 'https://api.nzpost.co.nz'

    @classmethod
    def is_configured(self):
        return (settings.NZPOST_OAUTH_CLIENT_ID is not None and
                settings.NZPOST_OAUTH_CLIENT_SECRET is not None)

    def extract_result(self, response, containerKey):
        if response.get("success") is not True:
            raise NZPostError(
                "Expected response to succeed, but it failed: %s" %
                response)
        results = response.get(containerKey)
        return None if (len(results) < 1) else results[0]

    @limits(calls=settings.NZPOST_RATE_LIMIT_RPS, period=ONE_SECOND)
    def request(self, path, params):
        try:
            return self.alf.get(self.host + path,
                                headers={
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'},
                                params=params)
        except RequestException as err:
            # insert a delay here in future
            raise NZPostError("NZ Post API request failed") from err

    def details(self, dpid):
        r = self.request('/addresschecker/1.0/details', {'dpid': dpid})
        return self.extract_result(r.json(), "details")

    def find(self, line1, line2="", line3="", line4=""):
        r = self.request('/addresschecker/1.0/find',
                         {
                             "address_line_1": line1,
                             "address_line_2": line2,
                             "address_line_3": line3,
                             "address_line_4": line4
                         })

        return self.extract_result(r.json(), "addresses")


class Geocoder:
    checker = AddressChecker()

    def geocode(self, address):
        result = self.checker.find(address)
        if result is None:
            return None
        dpid = result.get("DPID")
        result.update(self.checker.details(dpid))

        return result

    @classmethod
    def is_configured(cls):
        return AddressChecker.is_configured()
