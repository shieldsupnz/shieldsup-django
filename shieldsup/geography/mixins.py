import logging
import time

import django_rq
from django.db.models import Q
from django.conf import settings
from django.contrib.gis.geos import Point

from .models import CountryRegion, DistrictHealthBoardRegion
from .providers.nzpost import NZPostError, Geocoder

logger = logging.getLogger('geography')


class GeocodedModel:
    scheduler = django_rq.get_scheduler('default')

    def should_geocode(self):
        return ((self.latitude is None or self.longitude is None) and
                (self.geocoding_failures < settings.MAXIMUM_GEOCODING_ATTEMPTS))

    def clear_geocoded_fields(self):
        self.latitude = None
        self.longitude = None
        self.region = None
        self.dhb_region = None

    def geocoding_failure(self):
        self.geocoding_failures += 1

        logger.error('%s #%s failed to be geocoded (fail %d/%d)',
                     self._meta.object_name, self.pk, self.geocoding_failures,
                     settings.MAXIMUM_GEOCODING_ATTEMPTS)

        self.clear_geocoded_fields()
        self.save()

        if self.geocoding_failures >= settings.MAXIMUM_GEOCODING_ATTEMPTS:
            logger.error('%s #%s maximum retries exceeded (fail %d/%d)',
                         self._meta.object_name, self.pk,
                         self.geocoding_failures,
                         settings.MAXIMUM_GEOCODING_ATTEMPTS)
            return

        self.scheduler.enqueue_in(
            settings.GEOCODE_ATTEMPT_DELAY,
            self.perform_geocoding_job, self.pk)

    def perform_geocoding(self):
        if Geocoder.is_configured() is not True:
            logger.error('Geocoding not configured. Have you set '
                         'NZPOST_OAUTH_CLIENT_ID and '
                         'NZPOST_OAUTH_CLIENT_SECRET?')
            return
        try:
            if self.longitude is not None and self.latitude is not None:
                logger.info('%s #%s skipped as it has already been geocoded '
                            'to (%.4f, %.4f)',
                            self._meta.object_name, self.pk, self.latitude,
                            self.longitude)
                return

            geocoder = Geocoder()
            address = self.address.replace("\n", ",")
            address += ", " + self.city # pylint: disable=access-member-before-definition
            geocode_result = geocoder.geocode(address)

            if geocode_result is None:
                # as we will never find a different result
                # set straight to maximum failures and log
                logger.warning('%s #%s geocoding result not found '
                               'for address: %s. Ignoring...',
                               self._meta.object_name, self.pk, address)
                self.geocoding_failures = settings.MAXIMUM_GEOCODING_ATTEMPTS * 2
                self.clear_geocoded_fields()
                self.save()
                return

            self.delivery_address = geocode_result.get("FullAddress")

            if "CityTown" in geocode_result:
                self.city = geocode_result["CityTown"]
            elif "MailTown" in geocode_result:
                self.city = geocode_result["MailTown"]

            self.longitude, self.latitude = geocode_result.get(
                "NZGD2kCoord").get("coordinates")
            logger.info('%s #%s successfully geocoded to (%.4f, %.4f)',
                        self._meta.object_name, self.pk, self.latitude,
                        self.longitude)

            self.perform_regionize()
            self.save()
        except NZPostError:
            logger.exception('An exception occurred while requesting address info.')
            logger.info('Sleeping for 5 seconds...')
            time.sleep(5)
            self.geocoding_failure()

    def save(self, *args, **kwargs):
        # if there has been a change in the address or city
        if self.tracker.has_changed('address') or self.tracker.has_changed('city'):
            # reset the geocode fields
            self.geocoding_failures = 0
            self.clear_geocoded_fields()

        result = super().save(*args, **kwargs)
        # if there is no latitude or no longitude and failure is less than max
        if self.should_geocode() and Geocoder.is_configured() is True:
            django_rq.enqueue(type(self).perform_geocoding_job, self.pk)

        return result

    @classmethod
    def enqueue_geocoding_tasks(cls):

        query = cls.objects.exclude(Q(latitude=None) | Q(longitude=None))
        query = query.filter(Q(region=None))  #  | Q(dhb_region=None)
        for obj in query:
            logger.info("Enqueuing %s #%s...", cls.__name__, obj.pk)
            django_rq.enqueue(cls.perform_regionize_job, obj.pk)

        if Geocoder.is_configured() is not True:
            logger.error('Geocoding not configured. Have you set '
                         'NZPOST_OAUTH_CLIENT_ID and '
                         'NZPOST_OAUTH_CLIENT_SECRET?')
            return
        for obj in cls.objects.filter(
                latitude=None, longitude=None,
                geocoding_failures__lt=settings.MAXIMUM_GEOCODING_ATTEMPTS):
            logger.info("Enqueuing %s #%s...", cls.__name__, obj.pk)
            django_rq.enqueue(cls.perform_geocoding_job, obj.pk)

    @classmethod
    def perform_geocoding_job(cls, pk):
        """Gets model instance using primary key and starts geocoding"""
        if Geocoder.is_configured() is not True:
            logger.error('Geocoding not configured. Have you set '
                         'NZPOST_OAUTH_CLIENT_ID and '
                         'NZPOST_OAUTH_CLIENT_SECRET?')
            return
        try:
            obj = cls.objects.get(pk=pk)
            obj.perform_geocoding()
        except cls.DoesNotExist:
            logger.error('Cannot find %s with pk=%s', cls.__name__, pk)

    def can_regionize(self):
        return self.longitude is not None and self.latitude is not None

    def is_region_data_loaded(self):
        return (len(CountryRegion.objects.all()) > 0 and
                len(DistrictHealthBoardRegion.objects.all()) > 0)

    def perform_regionize(self):
        if not self.is_region_data_loaded():
            logger.error('Region data empty. Have you loaded the shapefiles yet?')
            return

        if not self.can_regionize():
            logger.warning('%s #%s cannot be regionized because it '
                           'does not have latitude / longitude coordinates.',
                           self._meta.object_name, self.pk)
            return

        point = Point(self.longitude, self.latitude)
        self.region = CountryRegion.objects.filter(geom__contains=point).first()
        query = DistrictHealthBoardRegion.objects.all()
        self.dhb_region = query.filter(geom__contains=point).first()

        if self.region is None:
            logger.warning('%s #%s region not found for coords: (%.4f, %.4f)',
                           self._meta.object_name, self.pk, self.latitude,
                           self.longitude)
        else:
            logger.info('%s #%s successfully geocoded to region %s',
                        self._meta.object_name, self.pk, self.region.name)

        if self.dhb_region is None:
            logger.warning('%s #%s dhb region not found for coords: (%.4f, %.4f)',
                           self._meta.object_name, self.pk, self.latitude,
                           self.longitude)
        else:
            logger.info('%s #%s successfully geocoded to dhb region %s',
                        self._meta.object_name, self.pk, self.dhb_region.name)

    @classmethod
    def perform_regionize_job(cls, pk):
        """Gets model instance using primary key and starts regionising"""
        try:
            obj = cls.objects.get(pk=pk)
            obj.perform_regionize()
            obj.save()
        except cls.DoesNotExist:
            logger.error('Cannot find %s with pk=%s', cls.__name__, pk)
