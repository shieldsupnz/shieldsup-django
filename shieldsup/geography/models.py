# This is an auto-generated Django model module created by ogrinspect.
from django.contrib.gis.db import models


class DistrictHealthBoardRegion(models.Model):
    category_code = "DHB"
    code = models.CharField(max_length=254)
    name = models.CharField(max_length=254)
    shape_leng = models.FloatField()
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return '{} ({}) [#{}]'.format(self.name, self.code, self.pk)


# Auto-generated `LayerMapping` dictionary for DistrictHealthBoardRegion model
districthealthboardregion_mapping = {
    'code': 'DHB2015_Co',
    'name': 'DHB2015_Na',
    'shape_leng': 'Shape_Leng',
    'geom': 'MULTIPOLYGON',
}


class RegionalCouncilRegion(models.Model):
    category_code = "RC"
    code = models.CharField(max_length=2)
    proper_name = models.CharField(max_length=254)
    name = models.CharField(max_length=200)
    land_area_field = models.FloatField()
    area_sq_km = models.FloatField()
    shape_leng = models.FloatField()
    geom = models.MultiPolygonField(srid=4326)


# Auto-generated `LayerMapping` dictionary for RegionalCouncilRegion model
regionalcouncilregion_mapping = {
    'code': 'REGC2020_V',
    'proper_name': 'REGC2020_1',
    'name': 'REGC2020_2',
    'land_area_field': 'LAND_AREA_',
    'area_sq_km': 'AREA_SQ_KM',
    'shape_leng': 'SHAPE_Leng',
    'geom': 'MULTIPOLYGON',
}


class AucklandCouncilBoardRegion(models.Model):
    category_code = "ACB"
    name = models.CharField(max_length=50)
    geom = models.MultiPolygonField(srid=4326)


# Auto-generated `LayerMapping` dictionary for AucklandCouncilBoardRegion model
aucklandcouncilboardregion_mapping = {
    'name': 'CB_NAME',
    'geom': 'MULTIPOLYGON',
}


class TerritorialAuthorityRegion(models.Model):
    category_code = "TA"
    code = models.CharField(max_length=3)
    proper_name = models.CharField(max_length=254)
    name = models.CharField(max_length=200)
    land_area_field = models.FloatField()
    area_sq_km = models.FloatField()
    shape_leng = models.FloatField()
    geom = models.MultiPolygonField(srid=4326)


# Auto-generated `LayerMapping` dictionary for TerritorialAuthorityRegion model
territorialauthorityregion_mapping = {
    'code': 'TA2020_V1_',
    'proper_name': 'TA2020_V_1',
    'name': 'TA2020_V_2',
    'land_area_field': 'LAND_AREA_',
    'area_sq_km': 'AREA_SQ_KM',
    'shape_leng': 'SHAPE_Leng',
    'geom': 'MULTIPOLYGON',
}


class CountryRegion(models.Model):
    "Subregion of the country."
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    kind = models.CharField(max_length=255, blank=True)
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return '{} ({}) [#{}]'.format(self.name, self.code, self.pk)
