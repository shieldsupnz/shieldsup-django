import os
import re

from django.conf import settings
from django.contrib.gis.utils import LayerMapping
from django.contrib.gis.geos import Point
from django.db import transaction

from contacts.models import Contact
from orders.models import Order

from .models import (
    CountryRegion,
    DistrictHealthBoardRegion, RegionalCouncilRegion,
    AucklandCouncilBoardRegion, TerritorialAuthorityRegion,
    districthealthboardregion_mapping, regionalcouncilregion_mapping,
    aucklandcouncilboardregion_mapping, territorialauthorityregion_mapping,
)


GISDATA_DIR = os.path.join(settings.BASE_DIR, 'gisdata')

district_health_board_2015_shp = os.path.abspath(
    os.path.join(GISDATA_DIR,
                 'district-health-board-2015',
                 'district-health-board-2015.shp'),
)

regional_council_2020_shp = os.path.abspath(
    os.path.join(GISDATA_DIR,
                 'regional-council-2020-generalised',
                 'regional-council-2020-generalised.shp'),
)

auckland_council_boards_2010_shp = os.path.abspath(
    os.path.join(GISDATA_DIR,
                 'auckland-council-boards-july-2010',
                 'auckland-council-boards-july-2010.shp'),
)

territorial_authority_2020_shp = os.path.abspath(
    os.path.join(GISDATA_DIR,
                 'territorial-authority-2020-generalised',
                 'territorial-authority-2020-generalised.shp'),
)


def run(verbose=True):
    lm = LayerMapping(DistrictHealthBoardRegion,
                      district_health_board_2015_shp,
                      districthealthboardregion_mapping,
                      transform=False)
    lm.save(strict=True, verbose=verbose)

    lm = LayerMapping(RegionalCouncilRegion,
                      regional_council_2020_shp,
                      regionalcouncilregion_mapping,
                      transform=False)
    lm.save(strict=True, verbose=verbose)

    lm = LayerMapping(AucklandCouncilBoardRegion,
                      auckland_council_boards_2010_shp,
                      aucklandcouncilboardregion_mapping,
                      transform=False)
    lm.save(strict=True, verbose=verbose)

    lm = LayerMapping(TerritorialAuthorityRegion,
                      territorial_authority_2020_shp,
                      territorialauthorityregion_mapping,
                      transform=False)
    lm.save(strict=True, verbose=verbose)
    generate_regions()


def generate_regions():
    with transaction.atomic():
        p_ta_name = re.compile("^(.+) (District|City|Territory)$")
        category_code = TerritorialAuthorityRegion.category_code
        for territory in TerritorialAuthorityRegion.objects.all():
            if territory.name == "Auckland":
                continue  # skip auckland for now
            if territory.name == "Area Outside Territorial Authority":
                continue  # ignore this one too

            # split name into name and kind (District, City, Territory)
            name = territory.name
            match = p_ta_name.match(name)
            if match is None:
                print('Ignoring territorial authority name "{}"'.format(name))
                continue
            name = match.group(1)
            kind = match.group(2)

            code = category_code + territory.code
            print("Loading %s - %s" % (code, name))
            CountryRegion.objects.update_or_create(
                code=code,
                defaults={
                    'name': name,
                    'kind': kind,
                    'geom': territory.geom
                }
            )
        auckland_ta = TerritorialAuthorityRegion.objects.get(name="Auckland")
        base_code = category_code + auckland_ta.code + "/"
        category_code = AucklandCouncilBoardRegion.category_code
        p_acb_name = re.compile(" Local Board Area$")
        for council_board in AucklandCouncilBoardRegion.objects.all():
            code = base_code + category_code + ('%03d' % council_board.pk)
            name = council_board.name
            # Remove Local Board Area suffix
            name = p_acb_name.sub("", name)

            print("Loading %s - Auckland / %s" % (code, name))
            CountryRegion.objects.update_or_create(
                code=code,
                defaults={
                    'name': 'Auckland / ' + name,
                    'kind': 'Local Board Area',
                    'geom': council_board.geom
                }
            )


def test():
    for customer in Contact.objects.all()[:10]:
        if customer.longitude is None or customer.latitude is None:
            print("Skipping {} because no coords".format(customer.name))
            continue
        point = Point(customer.longitude, customer.latitude)
        dhb_matches = DistrictHealthBoardRegion.objects.filter(geom__contains=point)
        rc_matches = CountryRegion.objects.filter(geom__contains=point)

        print("Customer '{} ({})".format(customer.name, point))
        print("  dhb: {}".format([dhb.name for dhb in dhb_matches]))
        print("  cr: {}".format([rc.name for rc in rc_matches]))

    for order in Order.objects.all()[:10]:
        if order.longitude is None or order.latitude is None:
            print("Skipping {} because no coords".format(order.customer.name))
            continue
        point = Point(order.longitude, order.latitude)
        dhb_matches = DistrictHealthBoardRegion.objects.filter(geom__contains=point)
        rc_matches = CountryRegion.objects.filter(geom__contains=point)

        print("Order '{} ({})".format(order.customer.name, point))
        print("  dhb: {}".format([dhb.name for dhb in dhb_matches]))
        print("  cr: {}".format([rc.name for rc in rc_matches]))
