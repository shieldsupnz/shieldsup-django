from django.contrib.gis import admin
from django.db.models import Count
# Register your models here.
from .models import CountryRegion, DistrictHealthBoardRegion



class CountryRegionHubInline(admin.TabularInline):
    model = CountryRegion.hubs.through
    extra = 0
    verbose_name = 'Hub'
    verbose_name_plural = 'Assigned hubs'

class DistrictHealthBoardRegionHubInline(admin.TabularInline):
    model = DistrictHealthBoardRegion.hubs.through
    extra = 0
    verbose_name = 'Hub'
    verbose_name_plural = 'Assigned hubs'

@admin.register(CountryRegion)
class CountryRegionAdmin(admin.GeoModelAdmin):
    modifiable = False
    readonly_fields = ('code', 'name', 'kind',)
    fields = ('code', 'name', 'kind', 'geom',)
    list_display = ('code', 'name', 'kind', 'hubs_assigned')
    search_fields = ('code', 'name', 'kind',)
    ordering = ('name',)
    inlines = (CountryRegionHubInline,)

    def hubs_assigned(self, obj):
        return obj.hub_count

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.defer('geom').annotate(hub_count=Count('hubs'))


@admin.register(DistrictHealthBoardRegion)
class DistrictHealthBoardRegionAdmin(admin.GeoModelAdmin):
    modifiable = False
    readonly_fields = ('code', 'name',)
    fields = ('code', 'name', 'geom',)
    list_display = ('code', 'name',)
    ordering = ('name',)
    inlines = (DistrictHealthBoardRegionHubInline,)

    def hubs_assigned(self, obj):
        return obj.hub_count

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.defer('geom').annotate(hub_count=Count('hubs'))
