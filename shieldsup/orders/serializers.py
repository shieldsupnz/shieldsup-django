from rest_framework import serializers
from orders.models import Order
from contacts.serializers import ContactSerializer


class OrderSerializer(serializers.ModelSerializer):
    customer = ContactSerializer(many=False, read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'customer', 'address', 'city', 'priority',
                  'quantity_needed', 'quantity_wanted', 'quantity_delivered',
                  'comments', 'assigned_to', 'state')
