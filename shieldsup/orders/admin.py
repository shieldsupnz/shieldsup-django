from datetime import timedelta

from django.contrib import admin
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.db.models import Count
from django.forms import Textarea

from common.admin import RegionFilter
from common.mixins import ExportCsv as ExportCsvMixin, AdminActionsMixin

from .models import Order

# Register your models here.


class ExportCsvOrderMixin(ExportCsvMixin):

    def export_as_csv(self, request, queryset):
        # select related but defer the geom field because they are slow
        queryset = queryset.select_related('region', 'dhb_region', 'customer')
        queryset = queryset.defer('region__geom', 'dhb_region__geom')
        return super().export_as_csv(request, queryset)
    export_as_csv.short_description = ExportCsvMixin.export_as_csv.short_description

    def model_csv_attributes(self):
        """Add email field to field names"""
        return [field.name for field in self.model._meta.fields] + ['email']

    def model_to_csv(self, model, field_names):
        """Include the email field manually"""
        return [getattr(model, field) for field in field_names if field !=
                'email'] + [model.customer.email]


class DuplicateCustomersFilter(admin.SimpleListFilter):
    title = _('duplicates')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'duplicates'

    def lookups(self, request, model_admin):
        return (
            ('customers', "Customer"),
            ('organizations', "Organization name"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'customers':
            query = Order.objects.values('customer_id')
            query = query.annotate(count=Count('customer_id'))
            query = query.filter(count__gt=1)
            dupe_customer_ids = query.values_list('customer_id', flat=True)
            return queryset.filter(customer_id__in=dupe_customer_ids)
        if self.value() == 'organizations':
            query = Order.objects.values('customer__organization')
            query = query.annotate(count=Count('customer__organization'))
            query = query.exclude(customer__organization='').filter(count__gt=1)
            dupe_orgs = query.values_list('customer__organization', flat=True)
            return queryset.filter(customer__organization__in=dupe_orgs)
        return queryset


class AssignedToFilter(admin.SimpleListFilter):
    title = _('assignment')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'assigned_to'

    def lookups(self, request, model_admin):
        return (
            ('me', "Assigned to me"),
            ('unassigned', "Unassigned"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'me':
            return queryset.filter(assigned_to__user=request.user)
        if self.value() == 'unassigned':
            return queryset.filter(assigned_to=None)

        return queryset


class AssignedAtFilter(admin.SimpleListFilter):
    title = _('assigned at')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'assigned_at'

    def lookups(self, request, model_admin):
        return (
            ('7days', "More than 7 days ago"),
        )

    def queryset(self, request, queryset):
        if self.value() == '7days':
            timestamp = timezone.now() - timedelta(days=7)
            return queryset.filter(assigned_at__lte=timestamp)
        return queryset



@admin.register(Order)
class OrderAdmin(admin.ModelAdmin, ExportCsvOrderMixin, AdminActionsMixin):
    fieldsets = (
        (_('Order info'), {
            'fields': (
                'customer', 'customer_phone', 'customer_email',
                'address', 'city', 'quantity_needed',
                'quantity_wanted', 'priority', 'comments',
            )
        }),
        (_('Manufacturer info'), {
            'fields': (
                'assigned_to', 'quantity_delivered', 'state', 'track_and_trace')
        }),
        (_('System'), {
            'fields': ('airtable_ref', 'data_source')
        }),
        (_('Location'), {
            'classes': ('collapse',),
            'fields': ('region', 'dhb_region', 'latitude', 'longitude',)
        }),
        (_('Timestamps'), {
            'fields': ('created_at', 'modified_at', 'assigned_at', 'shipped_at')
        }),
    )
    autocomplete_fields = ("customer", "assigned_to")
    list_display = (
        "id", "priority", "customer_name", "customer_phone",
        "customer_organization", "city", "region_name", "quantity_needed",
        "quantity_wanted", "quantity_delivered", "assigned_to_with_link",
        "state", "created_at", "modified_at", 'assigned_at',
    )
    list_display_links = list_display
    ordering = ('priority', 'created_at',)
    readonly_fields = (
        'created_at', 'modified_at', 'assigned_at', 'shipped_at', 'region',
        'latitude', 'longitude', 'data_source', 'airtable_ref',
        'customer_phone', 'customer_email', 'dhb_region',
    )
    search_fields = (
        "customer__name", "customer__email", "customer__organization",
        "customer__phone", "assigned_to__name", "id", "track_and_trace",
        "address", "city",
    )
    list_filter = (
        "state", 'priority', AssignedToFilter, AssignedAtFilter,
        "created_at", "city", RegionFilter)
    actions = ['export_as_csv']

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'address':
            formfield.widget = Textarea(attrs={'rows': 3, 'cols': 32})
        return formfield

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('region', 'dhb_region',
                               'customer', 'customer__contact_ptr',
                               'assigned_to')
        # defer the geom fields because they are huge
        qs = qs.defer('region__geom', 'dhb_region__geom')
        return qs

    def get_list_filter(self, request):
        list_filter = super().get_list_filter(request)
        good_groups = ['Admins', 'Hub Coordinators']
        group_match = request.user.groups.filter(name__in=good_groups).exists()
        if request.user.is_superuser or group_match:
            # Add the duplicate filter only if admin, hub coord or superuser
            return (DuplicateCustomersFilter,) + list_filter
        return list_filter

    def customer_name(self, obj):
        return mark_safe('{} <a href="{}">🔗</a>'.format(
            obj.customer.name,
            reverse("admin:contacts_customer_change", args=(obj.customer.pk,)),
        ))
    customer_name.short_description = "Name"
    customer_name.admin_order_field = 'customer__name'

    def customer_email(self, obj):
        return obj.customer.email
    customer_email.short_description = "Email"
    customer_email.admin_order_field = 'customer__email'

    def customer_phone(self, obj):
        return obj.customer.phone
    customer_phone.short_description = "Phone"
    customer_phone.admin_order_field = 'customer__phone'

    def customer_organization(self, obj):
        return obj.customer.organization
    customer_organization.short_description = "Organization"
    customer_organization.admin_order_field = 'customer__organization'

    def assigned_to_with_link(self, obj):
        if obj.assigned_to:
            return mark_safe('{} <a href="{}">🔗</a>'.format(
                obj.assigned_to.name,
                reverse("admin:contacts_manufacturer_change",
                        args=(obj.assigned_to.pk,)),
            ))
        return ""
    assigned_to_with_link.short_description = "Assigned to"
    assigned_to_with_link.admin_order_field = 'assigned_to__name'

    def region_name(self, obj):
        if obj.region is not None:
            return obj.region.name
        return '-'
    region_name.short_description = "Region name"
    region_name.admin_order_field = 'region__name'


# @admin.register(RejectedOrder)
class RejectedOrderAdmin(admin.ModelAdmin):
    list_display = (
        "airtable_ref", "data_source", "name", "email", "phone", "address",
        "city", "needed", "wanted", "modified_at",
    )
    list_display_links = list_display
    ordering = ('-modified_at',)
    search_fields = ("airtable_ref", "name", "email", "phone", "city",)
    readonly_fields = (
        "airtable_ref", "data", "name", "email", "phone", "address", "city",
        "needed", "wanted", "modified_at", "data_source"
    )
    list_filter = ('data_source',)
