from rest_framework import status

from common.tests import ShieldsUPAPITestCase


class OrderTests(ShieldsUPAPITestCase):
    def setUp(self):
        super().setUp()

        self.order_data = {
            'address': self.customer.address,
            'city': self.customer.city,
            'quantity_needed': 10,
            'quantity_wanted': 20,
        }

        self.order_assign_data = {
            'assigned_to': self.manufacturer.pk,
            'state': 'assigned',
        }

        self.order_ship_data = {
            'quantity_delivered': 20,
            'state': 'shipped',
        }

        self.invalid_order_data = {
            'customer': self.other_customer.pk,
            'address': self.customer.address,
            'city': self.customer.city,
            'quantity_needed': 40,
            'quantity_wanted': 100,
        }

    def test_order_without_authentication(self):
        response = self.client.get('/api/v1/orders/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post('/api/v1/orders/',
                                    self.order_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        pk = self.order.pk
        response = self.client.get('/api/v1/orders/{}/'.format(pk))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        pk = self.order.pk
        response = self.client.get('/api/v1/orders/{}/'.format(pk),
                                   self.order_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_order(self):
        self.client.force_authenticate(user=self.user)

        response = self.client.get('/api/v1/orders/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.post('/api/v1/orders/',
                                    self.order_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        pk = self.order.pk
        response = self.client.get('/api/v1/orders/{}/'.format(pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.patch('/api/v1/orders/{}/'.format(pk),
                                     self.order_assign_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.order.refresh_from_db()
        self.assertEqual(self.order.assigned_to, self.manufacturer)
        self.assertEqual(self.order.state, 'assigned')

        response = self.client.patch('/api/v1/orders/{}/'.format(pk),
                                     self.order_ship_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.order.refresh_from_db()
        self.assertEqual(self.order.quantity_delivered, 20)
        self.assertEqual(self.order.state, 'shipped')

        response = self.client.put('/api/v1/orders/{}/'.format(pk),
                                   self.invalid_order_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        other_pk = self.other_order.pk
        response = self.client.put('/api/v1/orders/{}/'.format(other_pk),
                                   self.order_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
