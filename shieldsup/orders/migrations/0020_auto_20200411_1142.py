# Generated by Django 3.0.5 on 2020-04-10 23:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0019_auto_20200411_1136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='priority',
            field=models.IntegerField(choices=[(3, 'High'), (2, 'Normal'), (1, 'Low')], default=2),
        ),
    ]
