# Generated by Django 3.0.5 on 2020-04-01 23:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0010_auto_20200401_2206'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='data_source',
            field=models.CharField(blank=True, default='django', max_length=255),
        ),
        migrations.AddField(
            model_name='rejectedorder',
            name='data_source',
            field=models.CharField(blank=True, default='django', max_length=255),
        ),
    ]
