# Generated by Django 3.0.5 on 2020-04-10 23:27

from django.db import migrations


def migrate_priorities(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Order = apps.get_model('orders', 'Order')

    priority_transform = {
        'high': 3,
        'normal' : 2,
        'low': 1,
    }

    for order in Order.objects.all():
        order.priority_new = priority_transform[order.priority]
        order.save()


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0016_order_priority_new'),
    ]

    operations = [
        migrations.RunPython(migrate_priorities),
    ]
