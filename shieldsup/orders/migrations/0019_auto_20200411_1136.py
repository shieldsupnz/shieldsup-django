# Generated by Django 3.0.5 on 2020-04-10 23:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0018_remove_order_priority'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='priority_new',
            new_name='priority',
        ),
    ]
