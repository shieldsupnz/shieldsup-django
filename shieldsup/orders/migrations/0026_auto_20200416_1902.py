# Generated by Django 3.0.5 on 2020-04-16 07:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('geography', '0003_countryregion_kind'),
        ('orders', '0025_auto_20200416_1725'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='dhb_region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='orders', to='geography.DistrictHealthBoardRegion', verbose_name='DHB region'),
        ),
        migrations.AlterField(
            model_name='order',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='orders', to='geography.CountryRegion'),
        ),
    ]
