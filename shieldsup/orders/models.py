from collections import defaultdict

from django.db import models
from django.db.models import Sum, Count
from django.utils import timezone

from model_utils import FieldTracker

from geography.mixins import GeocodedModel

# Create your models here.


class OrderState:
    WAITING = 'waiting'
    ASSIGNED = 'assigned'
    READY_TO_SHIP = 'ready'
    SHIPPED = 'shipped'
    INVALID = 'invalid'

    CHOICES = (
        (WAITING, 'Waiting'),
        (ASSIGNED, 'Assigned'),
        (READY_TO_SHIP, 'Ready to ship'),
        (SHIPPED, 'Shipped'),
        (INVALID, 'Invalid'),
    )


class OrderPriority:
    HIGH = 3
    NORMAL = 2
    LOW = 1

    CHOICES = (
        (HIGH, 'High'),
        (NORMAL, 'Normal'),
        (LOW, 'Low'),
    )

# These are big models
# pylint: disable=too-many-instance-attributes

class Order(GeocodedModel, models.Model):
    customer = models.ForeignKey('contacts.Customer', on_delete=models.CASCADE)
    address = models.TextField('Delivery address', blank=True)
    delivery_address = models.TextField(blank=True, editable=False)
    city = models.CharField('Delivery city', max_length=255, blank=True)
    priority = models.IntegerField(choices=OrderPriority.CHOICES,
                                   default=OrderPriority.NORMAL)
    # REPURPOSED FIELD May 9th 1pm
    quantity_needed = models.IntegerField("Standard shields wanted")
    # REPURPOSED FIELD May 9th 1pm
    quantity_wanted = models.IntegerField("Dental shields wanted")
    quantity_delivered = models.IntegerField("Total delivered", null=True, blank=True)
    state = models.CharField(max_length=255, choices=OrderState.CHOICES,
                             default=OrderState.WAITING)
    assigned_to = models.ForeignKey('contacts.Manufacturer',
                                    verbose_name="Assigned to",
                                    related_name='assigned_orders', null=True,
                                    blank=True, on_delete=models.SET_NULL)
    assigned_at = models.DateTimeField(null=True, blank=True)
    airtable_ref = models.CharField(max_length=255, blank=True, null=True)
    data_source = models.CharField(max_length=255, blank=True, default="django")
    track_and_trace = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    shipped_at = models.DateTimeField(null=True, blank=True)
    comments = models.TextField(blank=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    region = models.ForeignKey('geography.CountryRegion', null=True,
                               blank=True, on_delete=models.SET_NULL,
                               related_name="orders")
    dhb_region = models.ForeignKey('geography.DistrictHealthBoardRegion',
                                   verbose_name='DHB region',
                                   null=True, blank=True,
                                   on_delete=models.SET_NULL,
                                   related_name="orders")
    geocoding_failures = models.IntegerField(null=False, default=0,
                                             editable=False)

    tracker = FieldTracker()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._orig_assigned_to = self.assigned_to
        self._orig_state = self.state

    def save(self, *args, **kwargs):
        if self._state.adding:
            if self.assigned_to is not None:
                self.assigned_at = timezone.now()
            if self.state == OrderState.SHIPPED:
                self.shipped_at = timezone.now()
        else:
            if self.assigned_to != self._orig_assigned_to:
                if self.assigned_to is None:
                    self.assigned_at = None
                else:
                    self.assigned_at = timezone.now()
            if self.state != self._orig_state:
                if self.state == OrderState.SHIPPED:
                    self.shipped_at = timezone.now()
                else:
                    self.shipped_at = None
        if self.address in ['', None]:
            self.address = self.customer.address
            self.city = self.customer.city
            self.region = self.customer.region
            self.latitude = self.customer.latitude
            self.longitude = self.customer.longitude
        super().save(*args, **kwargs)
        self._orig_assigned_to = self.assigned_to
        self._orig_state = self.state

    def __repr__(self):
        return "{} ({}) [#{}]".format(self.customer.name, self.state, self.pk)
    __str__ = __repr__


class RejectedOrder(models.Model):
    airtable_ref = models.CharField(max_length=255)

    name = models.CharField(max_length=255, blank=True, editable=False)
    email = models.EmailField(blank=True, editable=False)
    phone = models.CharField(max_length=255, blank=True, editable=False)
    address = models.TextField(blank=True, editable=False)
    city = models.CharField(max_length=255, blank=True, editable=False)
    needed = models.IntegerField("Needed", default=0, editable=False)
    wanted = models.IntegerField("Wanted", default=0, editable=False)

    data_source = models.CharField(
        max_length=255, blank=True, default="django")
    data = models.TextField()
    modified_at = models.DateTimeField()

    def __repr__(self):
        return "#{} - Order {}".format(self.pk, self.airtable_ref)
    __str__ = __repr__


def get_order_metrics():
    """Compiles a list for order metrics

    Base paths:
        - /<region>/order

    Subkeys:
        - /<state_name>
            - /count
            - /quantity_needed
            - /quantity_wanted
            - /quantity_delivered
        - /TOTAL
            - /count
            - /quantity_needed
            - /quantity_wanted
            - /quantity_delivered
    """
    root_key = '/ALL/order'
    fieldnames = [
        'count', 'quantity_needed', 'quantity_delivered', 'quantity_wanted',
    ]
    data = {}
    states = list(map(lambda x: x[0], OrderState.CHOICES))
    query = Order.objects.values('state')
    results = query.annotate(count=Count('state'),
                             quantity_needed=Sum('quantity_needed'),
                             quantity_wanted=Sum('quantity_wanted'),
                             quantity_delivered=Sum('quantity_delivered'))
    totals = defaultdict(int)
    for state in states:
        base_key = root_key + '/' + state
        for field in fieldnames:
            key = base_key + '/' + field
            data[key] = 0
    for result in results:
        base_key = root_key + '/' + result['state']
        for field in fieldnames:
            key = base_key + '/' + field
            value = result[field] if result[field] is not None else 0
            data[key] = value
            totals[field] += value
    base_key = root_key + '/ALL'
    for field in fieldnames:
        key = base_key + '/' + field
        data[key] = totals[field]
    return data


def get_order_metrics_for_manufacturer(manufacturer):
    query = manufacturer.assigned_orders.all()
    query = query.values('state')
    query = query.annotate(quantity_delivered=Sum('quantity_delivered'),
                           count=Count('state'))
    # initialise data
    data = defaultdict(lambda: defaultdict(int))
    fieldnames = ('count', 'quantity_delivered')
    states = list(map(lambda x: x[0], OrderState.CHOICES))
    for state in states:
        for field in fieldnames:
            data[state][field] = 0
    # assemble data and sum totals
    for item in query:
        for field in fieldnames:
            value = item[field] or 0
            data[item['state']][field] = value
            data['ALL'][field] += value
    return data
