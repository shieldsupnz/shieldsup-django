from django.db.models import Q

from rest_framework.permissions import IsAuthenticated
from rest_framework import generics
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied

from contacts.models import Manufacturer
from .models import Order
from .serializers import OrderSerializer


class OrderPermission(permissions.BasePermission):
    message = 'Order access not allowed.'

    def has_object_permission(self, request, view, obj):
        manufacturer = Manufacturer.objects.get(user=request.user)
        return obj.assigned_to == manufacturer or obj.assigned_to is None


class OrderList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        manufacturer = Manufacturer.objects.get(user=self.request.user)
        return Order.objects.filter(Q(assigned_to=manufacturer) |
                                    Q(assigned_to=None))


class OrderRetrieveUpdate(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, OrderPermission)
    serializer_class = OrderSerializer

    def get_queryset(self):
        manufacturer = Manufacturer.objects.get(user=self.request.user)
        return Order.objects.filter(Q(assigned_to=manufacturer) |
                                    Q(assigned_to=None))

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        groups = ['Hub Coordinators', 'Admins']
        overrule = self.request.user.groups.filter(name__in=groups).exists()
        overrule = overrule or request.user.is_superuser
        if (not overrule and 'customer' in request.data and
                request.data['customer'] != instance.customer_id):
            raise PermissionDenied({
                "message": "cannot change customer on order",
                "object_id": int(request.data['customer']),
            })
        return super().update(request, *args, **kwargs)
