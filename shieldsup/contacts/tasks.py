
import csv
import logging
from collections import defaultdict

from django.db import transaction
from django.db.models import Count, Sum

from contacts.models import Contact, Manufacturer
from manufacturing.models import ThreeDimensionalPrinter
from orders.models import Order
from products.models import Product, Stock

logger = logging.getLogger('contacts')


def bulk_import_geocoded_contacts(input_path):
    infile = open(input_path, newline='')
    reader = csv.DictReader(infile)
    contact_stats = defaultdict(int)

    with transaction.atomic():
        for row in reader:
            # Get contact record
            try:
                contact = Contact.objects.get(id=row['id'])
                contact.latitude = row['latitude']
                contact.longitude = row['longitude']
                contact.address = row['address']
                contact.save()

                contact_stats['updated'] += 1
            except Contact.DoesNotExist:
                logger.warning('Contact #%s did not exist', row['id'])
                contact_stats['skipped'] += 1

    print("Successfully updated {} contacts, skipped {}.".format(
        contact_stats['updated'], contact_stats['skipped']))


def write_csv_manufacturer_stats(fp):
    # pylint: disable=too-many-locals

    # order related queries
    query = Order.objects.exclude(assigned_to=None)
    query = query.values('assigned_to', 'state')
    order_stats = query.annotate(count=Count('assigned_to'),
                                 delivered=Sum('quantity_delivered'))
    # product related queries
    product_names = {
        '3d Printed Headband for 3d Printed Face Shield': 'headband',
        '3d Printed Face Shield': 'completed_shield',
    }
    products = Product.objects.filter(name__in=product_names)
    product_lut = {p.pk: product_names[p.name] for p in products}
    # stock related
    stock_stats = Stock.objects.filter(
        product__in=products).values(
            'contact', 'product', 'quantity')
    # machine related
    machine_stats = ThreeDimensionalPrinter.objects.values(
        'owner', 'filament_diameter').annotate(
            count=Count('filament_diameter'))
    # order stats processing
    order_lut = defaultdict(lambda: defaultdict(int))
    for row in order_stats:
        orders = order_lut[row['assigned_to']]
        key_count = 'orders_assigned_to_{}'.format(row['state'])
        key_qty = 'orders_qty_delivered_{}'.format(row['state'])
        orders[key_count] = row['count']
        orders[key_qty] = row['delivered']
        orders['orders_assigned_to_total'] += row['count']
        # workaround for delivered being none
        orders['orders_qty_delivered_total'] += (row['delivered'] or 0)
        order_lut[row['assigned_to']] = orders
    # stock stats processing
    stock_lut = defaultdict(lambda: defaultdict(int))
    for row in stock_stats:
        stocks = stock_lut[row['contact']]
        key = 'stock_{}'.format(product_lut[row['product']])
        stocks[key] = row['quantity']
        stocks['stock_total'] += row['quantity']
        stock_lut[row['contact']] = stocks
    # machine stats processing
    machine_lut = defaultdict(lambda: defaultdict(int))
    for row in machine_stats:
        machines = machine_lut[row['owner']]
        key = 'machines_{}'.format(row['filament_diameter'].lower())
        machines[key] = row['count']
        machines['machines_total'] += row['count']
        machine_lut[row['owner']] = machines

    # headers for csv file
    headers = [
        'id', 'name', 'email', 'phone', 'address', 'city',
        # order counts
        'orders_assigned_to_total', 'orders_assigned_to_waiting', 'orders_assigned_to_assigned',
        'orders_assigned_to_ready', 'orders_assigned_to_shipped', 'orders_assigned_to_invalid',
        # order quantities delivered
        'orders_qty_delivered_total', 'orders_qty_delivered_waiting', 'orders_qty_delivered_assigned',
        'orders_qty_delivered_ready', 'orders_qty_delivered_shipped', 'orders_qty_delivered_invalid',
        # stock counts
        'stock_total', 'stock_headband', 'stock_completed_shield',
        # machine details
        'machines_total', 'machines_1.75mm', 'machines_3mm', 'machines_unknown',
    ]

    writer = csv.DictWriter(fp, fieldnames=headers)
    writer.writeheader()

    for person in Manufacturer.objects.all():
        stock = stock_lut.get(person.pk)
        machines = machine_lut.get(person.pk)
        orders = order_lut.get(person.pk)

        row = {
            'id': person.pk,
            'name': person.name,
            'email': person.email,
            'phone': person.phone,
            'address': person.address,
            'city': person.city
        }

        if orders is not None:
            row.update(orders)
        if stock is not None:
            row.update(stock)
        if machines is not None:
            row.update(machines)

        writer.writerow(row)
