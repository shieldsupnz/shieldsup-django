import logging

from django.core.management.base import BaseCommand

from contacts.tasks import bulk_import_geocoded_contacts


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('input_path', nargs=1)

    def handle(self, *args, **options):
        logging.basicConfig(level=logging.INFO)
        bulk_import_geocoded_contacts(options['input_path'][0])
        # Verbosity doesnt work
        logging.basicConfig(level=logging.INFO)
