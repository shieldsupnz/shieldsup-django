# Generated by Django 3.0.5 on 2020-04-16 07:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('geography', '0003_countryregion_kind'),
        ('contacts', '0009_auto_20200416_1725'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contact',
            options={'base_manager_name': 'objects'},
        ),
        migrations.AlterModelOptions(
            name='customer',
            options={'base_manager_name': 'objects'},
        ),
        migrations.AlterModelOptions(
            name='manufacturer',
            options={'base_manager_name': 'objects'},
        ),
        migrations.AlterModelOptions(
            name='supplier',
            options={'base_manager_name': 'objects'},
        ),
        migrations.AlterField(
            model_name='contact',
            name='dhb_region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='contacts', to='geography.DistrictHealthBoardRegion', verbose_name='DHB region'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='contacts', to='geography.CountryRegion'),
        ),
    ]
