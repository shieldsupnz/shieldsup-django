from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse
from django.utils import timezone

# Create your views here.
from .tasks import write_csv_manufacturer_stats


def admin_check(user):
    return user.is_superuser or user.groups.filter(name="Admins").exists()


@login_required
@user_passes_test(admin_check)
def get_csv_manufacturer_stats(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    timestamp = timezone.now().strftime("%Y-%m-%d_%H%M%S")
    filename = 'state_of_the_nation_{}.csv'.format(timestamp)
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
    write_csv_manufacturer_stats(response)
    return response
