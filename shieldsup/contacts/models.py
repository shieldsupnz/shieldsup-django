# Create your models here.

from django.conf import settings
from django.db import models

from model_utils import FieldTracker

from polymorphic.models import PolymorphicModel
from geography.mixins import GeocodedModel

# Create your models here.


class Contact(GeocodedModel, PolymorphicModel):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=255, blank=True)
    organization = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    delivery_address = models.TextField(blank=True, editable=False)
    city = models.CharField(max_length=255)
    comments = models.TextField(blank=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    region = models.ForeignKey('geography.CountryRegion', null=True,
                               blank=True, on_delete=models.SET_NULL,
                               related_name="contacts")
    dhb_region = models.ForeignKey('geography.DistrictHealthBoardRegion',
                                   verbose_name='DHB region',
                                   null=True, blank=True,
                                   on_delete=models.SET_NULL,
                                   related_name="contacts")
    geocoding_failures = models.IntegerField(null=False, default=0,
                                             editable=False)
    airtable_ref = models.CharField(max_length=255, null=True, blank=True)
    data_source = models.CharField(max_length=255, blank=True, default="django")
    is_active = models.BooleanField(default=True, blank=True)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, null=True,
                                blank=True, on_delete=models.SET_NULL,
                                related_name='contact')
    contact_person = models.ForeignKey(settings.AUTH_USER_MODEL,
                                       on_delete=models.SET_NULL,
                                       null=True,
                                       blank=True,
                                       related_name='customers_managed')
    last_contacted = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    tracker = FieldTracker()

    def __repr__(self):
        if not self.organization:
            return "{} [#{}]".format(self.name, self.pk)
        return "{} ({}) [#{}]".format(self.name, self.organization, self.pk)
    __str__ = __repr__


class Customer(Contact):
    is_medical_professional = models.BooleanField(default=False, blank=True)


class Manufacturer(Contact):
    can_courier_unaided = models.BooleanField(blank=True)


class Supplier(Contact):
    pass


def get_contact_metrics():
    """Compiles a dict of contact metrics

    Base paths:
        - /<region>/contact

    Subkeys:
        - /ALL/count
        - /customer/count
        - /manufacturer/count
        - /supplier/count
    """
    root_key = '/ALL/contact'
    data = {}
    data[root_key + '/customer/count'] = len(Customer.objects.all())
    data[root_key + '/manufacturer/count'] = len(Manufacturer.objects.all())
    data[root_key + '/supplier/count'] = len(Supplier.objects.all())
    data[root_key + '/ALL/count'] = sum(data.values())
    return data
