from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum, F, ExpressionWrapper, Value, FloatField, OuterRef, Subquery
from django.db.models.functions import Coalesce
from django.forms import Textarea
from polymorphic.admin import PolymorphicChildModelAdmin, PolymorphicParentModelAdmin

from common.admin import RegionFilter
from common.mixins import ExportCsv as ExportCsvMixin, AdminActionsMixin

from products.models import Product, StockTransfer
from orders.models import OrderState, Order
from .models import Contact, Customer, Manufacturer, Supplier


class ExportCsvContactMixin(ExportCsvMixin):

    def export_as_csv(self, request, queryset):
        # select related but defer the geom field because they are slow
        queryset = queryset.select_related('region', 'dhb_region',)
        queryset = queryset.defer('region__geom', 'dhb_region__geom')
        return super().export_as_csv(request, queryset)
    export_as_csv.short_description = ExportCsvMixin.export_as_csv.short_description


class ContactCityFilter(admin.SimpleListFilter):
    title = _('city')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'city'

    def lookups(self, request, model_admin):
        query = model_admin.model.objects.all().select_related('')
        results = query.values_list('city', flat=True).distinct().order_by('city')
        targets = zip(results, results)
        return targets

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        return queryset.filter(city=self.value())


class ContactPersonFilter(admin.SimpleListFilter):
    title = _('contact person')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'contactperson'

    def lookups(self, request, model_admin):
        return (
            ('me', "Assigned to me"),
            ('unassigned', "Unassigned"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'me':
            return queryset.filter(contact_person=request.user)
        if self.value() == 'unassigned':
            return queryset.filter(contact_person=None)

        return queryset


class ContactChildAdmin(PolymorphicChildModelAdmin):
    base_model = Contact
    ordering = ('name',)
    readonly_fields = (
        'created_at', 'modified_at', 'region', 'latitude', 'longitude',
        'data_source', 'airtable_ref', 'dhb_region',
    )
    list_filter = (
        "created_at", "modified_at", ContactPersonFilter,
        "last_contacted", "is_active", ContactCityFilter, RegionFilter
    )
    search_fields = (
        "name", "email", "organization", "airtable_ref", "phone", "city",
        "contact_person__email"
    )
    autocomplete_fields = ("contact_person", "user")
    base_fieldsets = (
        (_('Personal info'), {
            'fields': (
                'name', 'organization', 'email', 'phone', 'address', 'city',
                'comments'
            )
        }),
        (_('Designated contact person'), {
            'classes': ('collapse',),
            'fields': ('contact_person', 'last_contacted',)
        }),
        (_('System'), {
            'fields': ('user', 'is_active', 'airtable_ref', 'data_source')
        }),
        (_('Location'), {
            'classes': ('collapse',),
            'fields': ('region', 'dhb_region', 'latitude', 'longitude',)
        }),
        (_('Timestamps'), {
            'fields': ('created_at', 'modified_at')
        }),
    )

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'address':
            formfield.widget = Textarea(attrs={'rows': 3, 'cols': 32})
        return formfield

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('contact_person', 'user', 'region', 'dhb_region',)
        # defer the geom fields because they are huge
        qs = qs.defer('region__geom', 'dhb_region__geom')
        return qs

    def region_name(self, obj):
        if obj.region is not None:
            return obj.region.name
        return '-'
    region_name.short_description = "Region name"
    region_name.admin_order_field = 'region__name'


@admin.register(Customer)
class CustomerAdmin(ContactChildAdmin, ExportCsvContactMixin, AdminActionsMixin):
    base_model = Customer
    show_in_index = True
    list_display = (
        "id", "name", "email", "organization", "phone", "city", "region_name",
        "created_at", "modified_at",
    )
    list_display_links = list_display
    actions = ['export_as_csv']


@admin.register(Manufacturer)
class ManufacturerAdmin(ContactChildAdmin, ExportCsvContactMixin, AdminActionsMixin):
    base_model = Manufacturer
    show_in_index = True
    list_display = (
        "id", "name", "email", "phone", "city", "region_name",
        "frames_shipped", "frames_sent", "frames_recv", "frame_balance", "rolls_recv",
        "rolls_balance", "created_at",
    )
    list_display_links = list_display
    actions = ['export_as_csv']

    def frames_shipped(self, obj):
        return obj.frames_shipped
    frames_shipped.admin_order_field = 'frames_shipped'

    def frames_sent(self, obj):
        return obj.frames_sent
    frames_sent.admin_order_field = 'frames_sent'

    def frames_recv(self, obj):
        return obj.frames_recv
    frames_recv.admin_order_field = 'frames_recv'

    def frame_balance(self, obj):
        return obj.frame_balance
    frame_balance.admin_order_field = 'frame_balance'

    def rolls_recv(self, obj):
        return obj.rolls_recv
    rolls_recv.admin_order_field = 'rolls_recv'

    def rolls_balance(self, obj):
        return '%.2f' % obj.rolls_balance
    rolls_balance.admin_order_field = 'rolls_balance'

    def get_queryset(self, request):
        query = super().get_queryset(request)
        query = self.annotate_with_frame_roll_stats(query)
        return query


    def annotate_with_frame_roll_stats(self, query):
        frame = Product.objects.get(name__istartswith="frame")
        filaments = Product.objects.filter(name__istartswith="filament")
        RATIO_FRAME_FILAMENT = 1/30.

        query = query.prefetch_related('assigned_orders', 'stocktransfers_sent',
                                       'stocktransfers_received')
        query = query.annotate(
            frames_shipped=Coalesce(
                Subquery(
                    Order.objects.filter(
                        assigned_to=OuterRef('pk'),
                        state=OrderState.SHIPPED).
                    values('assigned_to').
                    annotate(total=Sum('quantity_delivered')).
                    values('total')
                ),
                Value(0)
            ),
            frames_sent=Coalesce(
                Subquery(
                    StockTransfer.objects.filter(
                        sender=OuterRef('pk'), product=frame).
                    values('sender').
                    annotate(total=Sum('quantity')).
                    values('total')
                ),
                Value(0)
            ),
            frames_recv=Coalesce(
                Subquery(
                    StockTransfer.objects.filter(
                        recipient=OuterRef('pk'),
                        product=frame).
                    values('recipient').
                    annotate(total=Sum('quantity')).
                    values('total')
                ),
                Value(0)
            ),
            rolls_recv=Coalesce(
                Subquery(
                    StockTransfer.objects.filter(
                        recipient=OuterRef('pk'),
                        product__in=filaments).
                    values('recipient').
                    annotate(total=Sum('quantity')).
                    values('total')
                ),
                Value(0)
            ),
            frame_balance=(F('frames_shipped')
                           + F('frames_sent')
                           - F('frames_recv')),
            rolls_balance=ExpressionWrapper(
                F('frame_balance') * RATIO_FRAME_FILAMENT - F('rolls_recv'),
                output_field=FloatField()
            )
        )
        return query


@admin.register(Supplier)
class SupplierAdmin(ContactChildAdmin, ExportCsvContactMixin, AdminActionsMixin):
    base_model = Supplier
    show_in_index = True
    list_display = (
        "id", "name", "email", "organization", "phone", "city", "region_name",
        "contact_person", "last_contacted", "created_at", "modified_at"
    )
    list_display_links = list_display
    actions = ['export_as_csv']


@admin.register(Contact)
class ContactAdmin(PolymorphicParentModelAdmin, ExportCsvContactMixin, AdminActionsMixin):
    base_model = Contact
    child_models = (Customer, Manufacturer, Supplier)
    ordering = ('name',)
    list_display = (
        "id", "name", "email", "organization", "phone", "city", "region_name",
        "contact_person", "last_contacted", "created_at", "modified_at",
        "region_name",
    )
    list_display_links = list_display
    search_fields = (
        "name", "email", "organization", "airtable_ref", "phone", "city",
        "contact_person__email", "region__name", "dhb_region__name"
    )
    actions = ['export_as_csv']
    list_filter = (
        "created_at", "modified_at", ContactPersonFilter,
        "last_contacted", "is_active", ContactCityFilter, RegionFilter
    )

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'address':
            formfield.widget = Textarea(attrs={'rows': 3, 'cols': 32})
        return formfield

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('contact_person', 'user', 'region', 'dhb_region',)
        # defer the geom fields because they are huge
        qs = qs.defer('region__geom', 'dhb_region__geom')
        return qs

    def region_name(self, obj):
        if obj.region is not None:
            return obj.region.name
        return '-'
    region_name.short_description = "Region name"
    region_name.admin_order_field = 'region__name'
