from django.contrib import admin
from django.db.models import Count

from contacts.models import Manufacturer
from geography.models import CountryRegion, DistrictHealthBoardRegion
# Register your models here.
from .models import Hub



@admin.register(Hub)
class HubAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'slack_channel', 'hub_coords', 'manufacturer_count',
        'regions_list',
    )
    list_filter = ('regions__name',)
    search_fields = (
        'regions__name', 'name', 'slack_channel', 'hub_coordinators__name',
    )

    fields = ('name', 'slack_channel', 'hub_coordinators', 'manufacturers',
              'regions', 'dhb_regions')

    filter_horizontal = ('manufacturers', 'hub_coordinators', 'regions',
                         'dhb_regions')

    def get_queryset(self, request):
        query = super().get_queryset(request)
        query = query.annotate(manufacturer_count=Count('manufacturers'))
        return query

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "regions":
            kwargs["queryset"] = CountryRegion.objects.defer('geom')
        elif db_field.name == "dhb_regions":
            kwargs["queryset"] = DistrictHealthBoardRegion.objects.defer('geom')
        elif db_field.name == "manufacturers":
            kwargs["queryset"] = Manufacturer.objects.all()
        elif db_field.name == "hub_coordinators":
            kwargs["queryset"] = Manufacturer.objects.all()

        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def hub_coords(self, obj):
        t = "{p.name} ({p.email})"
        people = [t.format(p=person) for person in obj.hub_coordinators.all()]
        return ", ".join(people)
    hub_coords.short_description = "Hub coordinators"

    def manufacturer_count(self, obj):
        return obj.manufacturer_count
    manufacturer_count.short_description = "Manufacturer count"
    manufacturer_count.admin_order_field = 'manufacturer_count'

    def regions_list(self, obj):
        names = obj.regions.values_list('name', flat=True)
        return ", ".join(names)
    regions_list.short_description = "Regions"
