from django.db import models

# Create your models here.

class Hub(models.Model):
    name = models.CharField(max_length=255)
    hub_coordinators = models.ManyToManyField('contacts.Manufacturer',
                                              blank=True,
                                              related_name='hubs_coordinated')
    regions = models.ManyToManyField('geography.CountryRegion', blank=True,
                                     related_name='hubs')
    dhb_regions = models.ManyToManyField('geography.DistrictHealthBoardRegion',
                                         blank=True, related_name='hubs',
                                         verbose_name='DHB regions')
    manufacturers = models.ManyToManyField('contacts.Manufacturer', blank=True,
                                           related_name='hubs_joined')
    slack_channel = models.CharField(max_length=255)
