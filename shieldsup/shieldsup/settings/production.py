

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

from corsheaders.signals import check_request_enabled

from .utils import config, secret

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = secret('django_secret_key')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [config('SERVER_IP'), config('APP_DOMAIN')]

# Trying to debug sloooow s3 static
STATIC_URL = '/static/'
STATIC_ROOT = '/static'

DEFAULT_FILE_STORAGE = 'shieldsup.custom_storages.MediaStorage'
AWS_S3_REGION_NAME = config('AWS_S3_REGION_NAME')
AWS_S3_ENDPOINT_URL = config('AWS_S3_ENDPOINT_URL')
AWS_ACCESS_KEY_ID = secret('aws_access_key_id')
AWS_SECRET_ACCESS_KEY = secret('aws_secret_access_key')
AWS_STORAGE_BUCKET_NAME = config('AWS_STORAGE_BUCKET_NAME')
AWS_S3_CUSTOM_DOMAIN = config('AWS_S3_CUSTOM_DOMAIN')
AWS_AUTO_CREATE_BUCKET = True
AWS_BUCKET_ACL = 'private'
AWS_DEFAULT_ACL = 'private'

AWS_LOCATION = 'web'
MEDIAFILES_LOCATION = AWS_LOCATION + '/media'

# EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
# ANYMAIL = {
#     "MAILGUN_API_KEY": secret('mailgun_api_key'),
#     "MAILGUN_SENDER_DOMAIN": config('MAILGUN_SENDER_DOMAIN'),
# }
# if config('MAILGUN_API_URL', default=None) is not None:
#     ANYMAIL['MAILGUN_API_URL'] = config('MAILGUN_API_URL')

DEFAULT_FROM_EMAIL = config('DEFAULT_FROM_EMAIL')
SERVER_EMAIL = config('SERVER_EMAIL')

NZPOST_OAUTH_CLIENT_ID=secret('nzpost_client_id')
NZPOST_OAUTH_CLIENT_SECRET=secret('nzpost_client_secret')
NZPOST_RATE_LIMIT_RPS=20


if secret('sentry_dsn', ignore_missing=True) is not None:
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration
    from sentry_sdk.integrations.rq import RqIntegration
    SENTRY_DSN = secret('sentry_dsn')
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        environment=config('ENVIRONMENT_NAME', default='development'),
        integrations=[DjangoIntegration(), RqIntegration()]
    )

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': config('POSTGRES_DB'),
        'USER': config('POSTGRES_USER'),
        'PASSWORD': secret('postgres_password'),
        'HOST': 'db',
        'PORT': '5432',
    }
}

CORS_ORIGIN_WHITELIST = (
    'https://' + config('AWS_S3_CUSTOM_DOMAIN'),
)


# Override CORS - Allow for /api/
def cors_allow_api_to_everyone(sender, request, **kwargs):
    return request.path.startswith('/api/')
check_request_enabled.connect(cors_allow_api_to_everyone)


REDIS_HOST = 'redis'
RQ_QUEUES = {
    'default': {
        'HOST': REDIS_HOST,
        'PORT': 6379,
        'DB': 0,
        'DEFAULT_TIMEOUT': 360,
    },
}

AIRTABLE_API_KEY = secret('airtable_api_key', ignore_missing=True)

SENDGRID_API_KEY = secret('sendgrid_api_key', ignore_missing=True)
if SENDGRID_API_KEY is not None:
    EMAIL_BACKEND = "anymail.backends.sendgrid.EmailBackend"
    ANYMAIL = {
        "SENDGRID_API_KEY": SENDGRID_API_KEY,
    }
