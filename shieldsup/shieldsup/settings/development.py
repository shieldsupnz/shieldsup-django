

import os

from corsheaders.signals import check_request_enabled

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/
from .utils import config

BASE_DIR = os.path.dirname(
    os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__))))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '(t0_u9!m%w6il^w*13os18x-hfqm#dxg3b&4tib#7%&toq-gkl'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*', '127.0.0.1']
CORS_ORIGIN_ALLOW_ALL = True
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'webroot', 'email')

DEFAULT_FROM_EMAIL = 'support@development.local'
SERVER_EMAIL = 'errors@development.local'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.spatialite',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

NZPOST_OAUTH_CLIENT_ID = config('NZPOST_OAUTH_CLIENT_ID', default=None)
NZPOST_OAUTH_CLIENT_SECRET = config('NZPOST_OAUTH_CLIENT_SECRET', default=None)
NZPOST_RATE_LIMIT_RPS=20

REDIS_HOST = 'localhost'
RQ_QUEUES = {
    'default': {
        'HOST': REDIS_HOST,
        'PORT': 6379,
        'DB': 0,
        'DEFAULT_TIMEOUT': 360,
    },
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/
MEDIA_URL = '/media/'
STATIC_URL = '/static/'

# STATIC_ROOT = os.path.join(BASE_DIR, 'webroot', 'static')

MEDIA_ROOT = os.path.join(BASE_DIR, 'webroot', 'media')

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny',
    ]
}

AIRTABLE_API_KEY = config('AIRTABLE_API_KEY', default=None)


# Override CORS - Allow for /api/
# Allow only GET and HEAD to start with
def cors_allow_api_to_everyone(sender, request, **kwargs):
    return request.path.startswith('/api/')
check_request_enabled.connect(cors_allow_api_to_everyone)
