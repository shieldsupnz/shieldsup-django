
import logging
import os

logger = logging.getLogger('settings')

BOOLEANS = {
    '1': True, 'yes': True, 'true': True, 'on': True,
    '0': False, 'no': False, 'false': False, 'off': False, '': False
}


def secret(secret_name, ignore_missing=False, secret_dir='/run/secrets'):
    """Get secret from storage

    Args:
        secret_name (str): The name of the secret to read.
        secret_dir (str, optional): The directory to read the secret from.
            Defaults to "/run/secrets"
    Returns:
        The value for secret_name
    Raises:
        FileNotFoundError: config not found
        ValueError: cannot cast config value
    """
    secret_path = '{}/{}'.format(secret_dir, secret_name)
    try:
        with open(secret_path, 'r') as fp:
            return fp.read().strip()
    except IOError as err:
        if isinstance(err, FileNotFoundError):
            err = FileNotFoundError(
                'Secret "{}" at "{}" not found'.format(
                    secret_name, secret_path))
        elif isinstance(err, PermissionError):
            err = PermissionError(
                'Secret "{}" at "{}" permission denied'.format(
                    secret_name, secret_path))

        if ignore_missing is True:
            logger.warning("%s. Returning None...", err)
            return None

        raise err


def config(config_name, cast=None, ignore_missing=False, **kwargs):  # pylint: disable=unused-argument
    """Get config from environment

    Args:
        config_name (str): The name of the config to read.
        cast (type, optional): The type to convert the read value into.
        default (bool, optional): The value to return when config is missing.
    Returns:
        The converted value for config_name
    Raises:
        KeyError: config not found
        ValueError: cannot cast config value
    """

    # get config from environment
    try:
        value = os.environ[config_name]
    except KeyError:
        if 'default' in kwargs:
            return kwargs['default']

        raise KeyError('Option "{}" not found in environment'.format(config_name))
    # cast to right type
    try:
        if cast is bool:
            value = BOOLEANS[value.lower()]
        elif cast is not None:
            value = cast(value)
    except BaseException:
        raise ValueError(
            'Cannot convert option "{}" value "{}" to a {}'.format(
                config_name, value, cast.__name__))
    return value
