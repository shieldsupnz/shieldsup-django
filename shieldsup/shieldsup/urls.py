"""shieldsup URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib.gis import admin
from django.contrib.auth.views import (
    PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView,
    PasswordResetCompleteView
)
from django.urls import path, include
from django.views.generic.base import RedirectView

from backend.views import MedicalRequest
from users.views import UserCreate, CustomAuthToken
from contacts.views import get_csv_manufacturer_stats
from metrics.views import metrics_historical_view, metrics_current_view, metrics_for_user_current_view
from common.views import static_view

admin.site.site_title = 'ShieldsUp'
admin.site.site_header = admin.site.site_title


urlpatterns = [
    path('admin/password_reset/', PasswordResetView.as_view(), name='admin_password_reset'),
    path('admin/password_reset/done/', PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='admin/', permanent=False), name='index'),
    path('api/v1/orders/webhook', MedicalRequest.as_view()),
    path('api/v1/users/webhook', UserCreate.as_view()),
    path('api/v1/misc/manufacturer-stats-csv/', get_csv_manufacturer_stats),
    path('api/v1/metrics/', metrics_current_view),
    path('api/v1/metrics/<isodate>', metrics_historical_view),
    path('api/v1/metrics/user/', metrics_for_user_current_view),
    path('status/', static_view('status.html')),
    path('api/v1/auth/login/', CustomAuthToken.as_view()),
    path('api/v1/', include('orders.urls')),
    path('api/v1/', include('products.urls')),
]
