from rest_framework import serializers
from products.models import Product, Stock, StockRequest
from contacts.serializers import ContactSerializer
from users.serializers import UserSerializer


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'name', 'kind', 'description']


class StockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stock
        fields = (
            'id', 'contact', 'product', 'quantity', 'created_at', 'modified_at'
        )

    def create(self, validated_data):
        """Creates or updates the stock"""
        product = validated_data.pop('product')
        contact = validated_data.pop('contact')
        obj, _ = Stock.objects.update_or_create(product=product, contact=contact,
                                                defaults=validated_data)
        return obj

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['product'] = ProductSerializer(instance.product).data
        return response

    def get_unique_together_validators(self):
        """Overriding method to disable unique together checks"""
        return []


class StockRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = StockRequest
        fields = (
            'id', 'manufacturer', 'product', 'quantity_requested',
            'quantity_given', 'request_comments', 'response_comments',
            'assigned_to', 'priority', 'state', 'created_at', 'modified_at'
        )

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['manufacturer'] = ContactSerializer(instance.manufacturer).data
        response['product'] = ProductSerializer(instance.product).data
        response['assigned_to'] = UserSerializer(instance.assigned_to).data
        return response
