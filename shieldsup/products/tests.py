from copy import copy

from rest_framework import status

from common.tests import ShieldsUPAPITestCase

from .models import Stock, StockRequest


class StockTests(ShieldsUPAPITestCase):  # pylint: disable=too-many-instance-attributes
    def setUp(self):
        super().setUp()
        self.stock_data = {
            'product': self.product.pk,
            'contact': self.manufacturer.pk,
            'quantity': 100,
        }

        self.other_stock_data = {
            'product': self.product.pk,
            'contact': self.other_manufacturer.pk,
            'quantity': 100,
        }

    def test_stock_without_authentication(self):
        response = self.client.get('/api/v1/stock/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post('/api/v1/stock/', data=self.stock_data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_stock(self):
        self.client.force_authenticate(user=self.user)

        response = self.client.get('/api/v1/stock/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(Stock.objects.filter(contact=self.manufacturer)), 0)

        response = self.client.post('/api/v1/stock/', self.stock_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(Stock.objects.filter(contact=self.manufacturer)), 1)

        response = self.client.post('/api/v1/stock/', self.stock_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(Stock.objects.filter(contact=self.manufacturer)), 1)

        response = self.client.post('/api/v1/stock/', self.other_stock_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(len(Stock.objects.filter(contact=self.manufacturer)), 1)


class StockRequestTests(ShieldsUPAPITestCase):  # pylint: disable=too-many-instance-attributes
    def setUp(self):
        super().setUp()

        self.stockrequest_data = {
            'product': self.product.pk,
            'manufacturer': self.manufacturer.pk,
            'quantity_requested': 100,
        }

        self.other_stockrequest_data = {
            'product': self.product.pk,
            'manufacturer': self.other_manufacturer.pk,
            'quantity_requested': 100,
        }

    def test_stockrequest_without_authentication(self):
        response = self.client.get('/api/v1/stockrequests/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post('/api/v1/stockrequests/',
                                    self.stockrequest_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.get('/api/v1/stockrequests/1/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post('/api/v1/stockrequests/1/',
                                    self.stockrequest_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_stockrequests(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get('/api/v1/stockrequests/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        length = len(StockRequest.objects.filter(manufacturer=self.manufacturer))
        self.assertEqual(length, 0)
        response = self.client.post('/api/v1/stockrequests/',
                                    self.stockrequest_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        length = len(StockRequest.objects.filter(manufacturer=self.manufacturer))
        self.assertEqual(length, 1)
        response = self.client.post('/api/v1/stockrequests/',
                                    self.stockrequest_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        length = len(StockRequest.objects.filter(manufacturer=self.manufacturer))
        self.assertEqual(length, 2)

        response = self.client.get('/api/v1/stockrequests/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = copy(self.stockrequest_data)
        data['quantity_requested'] = 1000
        stock_request = StockRequest.objects.first()
        response = self.client.put('/api/v1/stockrequests/{}/'.format(stock_request.pk),
                                   self.stockrequest_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        length = len(StockRequest.objects.filter(manufacturer=self.manufacturer))
        self.assertEqual(length, 2)

        response = self.client.post('/api/v1/stockrequests/',
                                    self.other_stockrequest_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        length = len(StockRequest.objects.filter(manufacturer=self.manufacturer))
        self.assertEqual(length, 2)
