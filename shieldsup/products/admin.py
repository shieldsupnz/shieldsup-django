from django.contrib import admin
from django.db.models import Q
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from contacts.models import Contact, Manufacturer, Supplier

from .models import (
    Product, Stock, StockRequest, StockRequestState, StockTransfer
)

# Register your models here.



class StockCityFilter(admin.SimpleListFilter):
    """Excludes Customer cities from filter"""
    title = _('city')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'city'

    def lookups(self, request, model_admin):
        query = Contact.objects.instance_of(Manufacturer)
        query |= Contact.objects.instance_of(Supplier)
        results = query.values_list('city', flat=True).distinct().order_by('city')
        targets = zip(results, results)
        return targets

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        return queryset.filter(contact__city=self.value())


class StockRequestMineOnlyFilter(admin.SimpleListFilter):
    title = _('person')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'owner'

    def lookups(self, request, model_admin):
        return (
            ('me', "Just mine"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'me':
            return queryset.filter(Q(manufacturer__user=request.user) |
                                   Q(assigned_to=request.user))
        return queryset

# Register your models here.


class MineOnlyFilter(admin.SimpleListFilter):
    title = _('owner')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'owner'

    def lookups(self, request, model_admin):
        return (
            ('me', "Just mine"),
        )

    def queryset(self, request, queryset):
        if self.value() == 'me':
            return queryset.filter(contact__user=request.user)
        return queryset


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        "id", "name", "description", "kind", "created_at", "modified_at"
    )
    list_display_links = list_display
    ordering = ('name',)
    readonly_fields = ('created_at', 'modified_at')
    search_fields = ("name",)
    list_filter = ("kind", "created_at", "modified_at")


@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):
    autocomplete_fields = ("product", "contact")
    list_display = (
        "id", "contact_name", "contact_organization", "contact_city",
        "product_name", "product_kind", "quantity", "created_at", "modified_at"
    )
    list_display_links = list_display
    ordering = ('-modified_at',)
    readonly_fields = ('created_at', 'modified_at')
    search_fields = (
        "contact__name", "contact__organization",
        "product__name", "product__kind"
    )
    list_select_related = ("product", "contact")
    list_filter = (
        "product__kind", "product__name", MineOnlyFilter,
        "created_at", "modified_at", StockCityFilter,
    )

    def contact_name(self, obj):
        return mark_safe('{} <a href="{}">🔗</a>'.format(
            obj.contact.name,
            reverse("admin:contacts_contact_change", args=(obj.contact.pk,)),
        ))
    contact_name.short_description = "Contact Name"

    def contact_organization(self, obj):
        return obj.contact.organization
    contact_organization.short_description = "Organization"

    def contact_city(self, obj):
        return obj.contact.city
    contact_city.short_description = "City"

    def product_name(self, obj):
        return obj.product.name
    product_name.short_description = "Product Name"

    def product_kind(self, obj):
        return obj.product.get_kind_display()
    product_kind.short_description = "Kind"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'contact':
            query = Manufacturer.objects.filter(user=request.user).first()
            kwargs["initial"] = query
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(StockRequest)
class StockRequestAdmin(admin.ModelAdmin):
    autocomplete_fields = ("manufacturer", "product", "assigned_to")
    list_display = (
        "id", "priority", "requester_name", "requester_city", "product_name",
        "product_kind", "quantity_requested", "quantity_given",
        "assigned_to_name", "state", "created_at", "modified_at"
    )
    list_display_links = list_display
    ordering = ('-modified_at',)
    readonly_fields = ('created_at', 'modified_at')
    search_fields = (
        "manufacturer__name", "manufacturer__email", "product__name",
        "assigned_to__last_name", "assigned_to__first_name",
        "assigned_to__email", "manufacturer__city", "product__kind"
    )
    list_select_related = ("manufacturer", "assigned_to", "product")
    list_filter = (
        "state", "priority", StockRequestMineOnlyFilter, "product__name",
        "product__kind", "created_at", "modified_at", "manufacturer__city"
    )
    fieldsets = (
        (_('Request info'), {
            'fields': (
                'manufacturer', 'product', 'quantity_requested', 'request_comments',
            )
        }),
        (_('Response info'), {
            'fields': (
                'state', 'priority', 'quantity_given', 'assigned_to', 'response_comments',
            )
        }),
        (_('Timestamps'), {
            'fields': ('created_at', 'modified_at')
        }),
    )

    def requester_name(self, obj):
        return mark_safe('{} <a href="{}">🔗</a>'.format(
            obj.manufacturer.name,
            reverse(
                "admin:contacts_manufacturer_change", args=(
                    obj.manufacturer.pk,)),
        ))

    def requester_city(self, obj):
        return obj.manufacturer.city

    def product_name(self, obj):
        return obj.product.name

    def product_kind(self, obj):
        return obj.product.kind

    def assigned_to_name(self, obj):
        if obj.assigned_to is None:
            return None
        return obj.assigned_to.first_name + " " + obj.assigned_to.last_name

    # breaks if there is no manufacturer with user set as current user
    # this should be stopgapped by has_module_permission
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        good_groups = ['Admins', 'Hub Coordinators']
        group_match = request.user.groups.filter(name__in=good_groups).exists()
        special_person = (request.user.is_superuser or group_match)
        if db_field.name == 'manufacturer' and not special_person:
            query = Manufacturer.objects.filter(user=request.user).first()
            kwargs["initial"] = query
            # making the field readonly
            kwargs['disabled'] = True
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        good_groups = ['Admins', 'Hub Coordinators']
        group_match = request.user.groups.filter(name__in=good_groups).exists()
        special_person = (request.user.is_superuser or group_match)
        if not special_person:
            obj.manufacturer = Manufacturer.objects.get(user=request.user)
        super().save_model(request, obj, form, change)

    def has_module_permission(self, request):
        result = super().has_module_permission(request)
        # check to see if the user is actually logged in
        if not request.user.is_authenticated:
            return result
        good_groups = ['Admins', 'Hub Coordinators']
        group_match = request.user.groups.filter(name__in=good_groups).exists()
        good_person = (request.user.is_superuser or group_match or
                       Manufacturer.objects.filter(user=request.user).exists())
        return result and good_person

    def has_add_permission(self, request):
        has_add_permission = super().has_add_permission(request)
        good_groups = ['Admins', 'Hub Coordinators']
        group_match = request.user.groups.filter(name__in=good_groups).exists()
        valid_person = (request.user.is_superuser or group_match or
                        Manufacturer.objects.filter(user=request.user).exists())
        return has_add_permission and valid_person

    def has_change_permission(self, request, obj=None):
        has_change_permission = super().has_change_permission(request, obj)
        good_groups = ['Admins', 'Hub Coordinators']
        group_match = request.user.groups.filter(name__in=good_groups).exists()
        valid_person = (request.user.is_superuser or group_match or
                        obj is None or obj.manufacturer.user == request.user)
        return has_change_permission and valid_person

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields = list(readonly_fields)
        good_groups = ['Admins', 'Hub Coordinators']
        group_match = request.user.groups.filter(name__in=good_groups).exists()
        special_person = (request.user.is_superuser or group_match)
        if special_person:
            return readonly_fields

        readonly_fields += [
            'assigned_to', 'priority', 'state', 'quantity_given',
            'response_comments'
        ]

        if obj is None:
            return readonly_fields
        if obj.state != StockRequestState.WAITING:
            readonly_fields += ['quantity_requested']
        readonly_fields += ['manufacturer']
        return readonly_fields


@admin.register(StockTransfer)
class StockTransferAdmin(admin.ModelAdmin):
    ordering = ('-modified_at',)
    list_display = (
        "id", "recipient", "sender", "product", "quantity",
        "created_by", "created_at", "modified_at",
    )
    list_display_links = list_display
    search_fields = (
        "sender__name", "recipient__name", "product__name",
    )
    actions = ['export_as_csv']
    list_filter = (
        "created_at", "modified_at", "product__name",
    )
    autocomplete_fields = ('product', 'sender', 'recipient')
    readonly_fields = ("created_by", "modified_by", "created_at", "modified_at")
    fieldsets = (
        (None, {
            'fields': (
                'product', 'sender', 'recipient', 'quantity', 'comments',
            )
        }),
        (_('Timestamps'), {
            'classes': ('collapse',),
            'fields': (('created_at', 'created_by'), ('modified_at', 'modified_by'))
        }),
    )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('sender', 'recipient', 'product', 'created_by',
                               'modified_by')
        return qs

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        obj.modified_by = request.user
        super().save_model(request, obj, form, change)
