from collections import defaultdict

from django.db import models
from django.db.models import Sum, Count

# Create your models here.


class ProductKind:
    PRODUCT_CONSUMABLE = 'consumable'
    PRODUCT_MANUFACTURED = 'manufactured'
    PRODUCT_FINISHED_PRODUCT = 'finished'

    CHOICES = (
        (PRODUCT_CONSUMABLE, 'Consumable'),
        (PRODUCT_MANUFACTURED, 'Manufactured part'),
        (PRODUCT_FINISHED_PRODUCT, 'Finished product'),
    )


class StockRequestPriority:
    HIGH = 3
    NORMAL = 2
    LOW = 1

    CHOICES = (
        (HIGH, 'High'),
        (NORMAL, 'Normal'),
        (LOW, 'Low'),
    )


class StockRequestState:
    WAITING = 'waiting'
    ACCEPTED = 'accepted'
    SHIPPED = 'shipped'
    COMPLETE = 'complete'
    REFUSED = 'refused'

    CHOICES = (
        (WAITING, 'Waiting'),
        (ACCEPTED, 'Accepted'),
        (SHIPPED, 'Shipped'),
        (COMPLETE, 'Complete'),
        (REFUSED, 'Refused'),
    )


class Product(models.Model):
    name = models.CharField(max_length=255, unique=True)
    kind = models.CharField(max_length=255, choices=ProductKind.CHOICES)
    description = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return "{} ({}) [#{}]".format(self.name, self.kind, self.pk)
    __str__ = __repr__


class Stock(models.Model):
    contact = models.ForeignKey('contacts.Contact', on_delete=models.PROTECT,
                                related_name='stocks')
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    quantity = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return "{} ({}) [#{}]".format(self.product.name, self.contact.name,
                                      self.pk)
    __str__ = __repr__

    class Meta:
        unique_together = ('contact', 'product')


class StockRequest(models.Model):
    manufacturer = models.ForeignKey('contacts.Manufacturer',
                                     on_delete=models.PROTECT,
                                     related_name='stock_requests',
                                     help_text='The person who is requesting the stock')
    # hub = models.ForeignKey('hubs.Hub', on_delete=models.PROTECT)
    product = models.ForeignKey('products.Product', on_delete=models.PROTECT,
                                help_text='The product you would like to receive')
    quantity_requested = models.IntegerField()
    quantity_given = models.IntegerField(blank=True, null=True)
    request_comments = models.TextField(blank=True)
    response_comments = models.TextField(blank=True,
                                         help_text='Please leave a note here if you reject the request')
    assigned_to = models.ForeignKey('users.User', null=True, on_delete=models.SET_NULL,
                                    help_text='The user who is dealing with the request')
    priority = models.IntegerField(choices=StockRequestPriority.CHOICES, default=StockRequestPriority.NORMAL)
    state = models.CharField(max_length=255,
                             choices=StockRequestState.CHOICES,
                             default=StockRequestState.WAITING,
                             help_text='Please leave a note in response comments you reject the request')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return "{} for {} ({}) [#{}]".format(self.product.name,
                                             self.manufacturer.name,
                                             self.state,
                                             self.pk)
    __str__ = __repr__


class StockTransfer(models.Model):
    product = models.ForeignKey('products.Product',
                                on_delete=models.CASCADE,
                                related_name='+',
                                help_text='The stock being sent')
    sender = models.ForeignKey('contacts.Manufacturer',
                               on_delete=models.CASCADE,
                               related_name='stocktransfers_sent',
                               help_text='The person who is sending the stock')
    recipient = models.ForeignKey('contacts.Manufacturer',
                                  on_delete=models.CASCADE,
                                  related_name='stocktransfers_received',
                                  help_text='The person who is receiving the stock')
    quantity = models.IntegerField(help_text='How much of the stock was sent')
    comments = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('users.User', on_delete=models.PROTECT,
                                   null=True, blank=True, related_name='+')
    modified_by = models.ForeignKey('users.User', on_delete=models.PROTECT,
                                    null=True, blank=True, related_name='+')

    def __str__(self):
        return '{} -> {} ({} - {} pcs) [#{}]'.format(self.sender.name,
                                                     self.recipient.name,
                                                     self.product.name,
                                                     self.quantity,
                                                     self.pk)


def get_product_metrics_labels():
    data = {}
    for product in Product.objects.all():
        data[str(product.pk)] = {
            'kind': product.kind,
            'name': product.name,
        }
    return {'products': data}

def get_product_metrics():  # pylint: disable=too-many-locals, too-many-branches
    """Compiles a dict of product / stock related metrics

    Base paths:
        - /<region>/product

    Subkeys:
        - /stock/<kind>/<product_id>
        - /stockrequest
            - ALL
                - count
                - total_quantity_requested
                - total_quantity_given
            - state/<state>
                - count
                - total_quantity_requested
                - total_quantity_given
            - product/<kind>/<product_id>
                - /ALL
                    - count
                    - total_quantity_requested
                    - total_quantity_given
                - state/<state>
                    - count x
                    - total_quantity_requested
                    - total_quantity_given
    """
    root_key = '/ALL/product'
    fieldnames = ['count', 'total_quantity_requested', 'total_quantity_given']
    data = {}
    states = list(map(lambda x: x[0], StockRequestState.CHOICES))
    query = Stock.objects.values('product_id', 'product__name', 'product__kind')
    stock_counts = query.annotate(total_quantity=Sum('quantity'))
    for sc in stock_counts:
        bits = [root_key, 'stock', str(sc['product_id'])]
        local_key = '/'.join(bits)
        data[local_key] = sc['total_quantity']
    totals = defaultdict(int)
    state_totals = defaultdict(lambda: defaultdict(int))
    product_totals = defaultdict(lambda: defaultdict(int))
    query = StockRequest.objects.values('product_id', 'product__name',
                                        'product__kind', 'state')
    request_counts = query.annotate(count=Count('state'),
                                    total_quantity_requested=Sum('quantity_requested'),
                                    total_quantity_given=Sum('quantity_given'))
    product_ids = []
    for src in request_counts:
        bits = [
            root_key, 'stockrequest', 'product',
            str(src['product_id'])
        ]
        local_key = '/'.join(bits)
        if src['product_id'] not in product_ids:
            product_ids += [src['product_id']]
            for state in states:
                for field in fieldnames:
                    key = '/'.join([local_key, state, field])
                    data[key] = 0
        local_key = local_key + '/' + src['state']
        for field in fieldnames:
            key = local_key + '/' + field
            value = src[field] or 0
            data[key] = value
            product_path = str(src['product_id'])
            product_totals[product_path][field] += value
            state_totals[src['state']][field] += value
            totals[field] += value
    for product_path, subdata in product_totals.items():
        bits = [root_key, 'stockrequest/product', product_path, 'ALL']
        local_key = '/'.join(bits)
        for field in subdata:
            key = local_key + '/' + field
            data[key] = subdata[field]
    for state in states:
        if state not in state_totals:
            subdata = defaultdict(int)
            for field in fieldnames:
                subdata[field] = 0
        else:
            subdata = state_totals[state]
        bits = [root_key, 'stockrequest/state', state]
        local_key = '/'.join(bits)
        for field in subdata:
            key = local_key + '/' + field
            data[key] = subdata[field]
    local_key = root_key + '/stockrequest/ALL'
    for field, value in totals.items():
        key = local_key + '/' + field
        data[key] = value
    return data
