from rest_framework import generics
from rest_framework import permissions
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from contacts.models import Manufacturer
from .models import Product, Stock, StockRequest
from .serializers import (
    ProductSerializer, StockSerializer, StockRequestSerializer
)


class StockRequestPermission(permissions.BasePermission):
    message = 'Stock request access not allowed.'

    def has_object_permission(self, request, view, obj):
        manufacturer = Manufacturer.objects.get(user=request.user)
        return obj.manufacturer == manufacturer


class ProductList(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class StockListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = StockSerializer

    def get_queryset(self):
        manufacturer = Manufacturer.objects.get(user=self.request.user)
        return Stock.objects.filter(contact=manufacturer)

    def create(self, request):
        manufacturer = Manufacturer.objects.get(user=self.request.user)
        groups = ['Hub Coordinators', 'Admins']
        overrule = self.request.user.groups.filter(name__in=groups).exists()
        overrule = overrule or request.user.is_superuser
        if not overrule and request.data['contact'] != manufacturer.pk:
            raise PermissionDenied({
                "message": "contact is not assigned to current user",
                "object_id": int(request.data['contact']),
            })
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)


class StockRequestListCreate(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, StockRequestPermission,)
    serializer_class = StockRequestSerializer

    def get_queryset(self):
        manufacturer = Manufacturer.objects.get(user=self.request.user)
        return StockRequest.objects.filter(manufacturer=manufacturer)

    def create(self, request):
        manufacturer = Manufacturer.objects.get(user=self.request.user)
        groups = ['Hub Coordinators', 'Admins']
        overrule = self.request.user.groups.filter(name__in=groups).exists()
        overrule = overrule or request.user.is_superuser
        if not overrule and request.data['manufacturer'] != manufacturer.pk:
            raise PermissionDenied({
                "message": "manufacturer is not assigned to the current user",
                "object_id": int(request.data['manufacturer']),
            })
        return super().create(request)


class StockRequestRetrieveUpdate(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, StockRequestPermission,)
    serializer_class = StockRequestSerializer

    def get_queryset(self):
        manufacturer = Manufacturer.objects.get(user=self.request.user)
        return StockRequest.objects.filter(manufacturer=manufacturer)
