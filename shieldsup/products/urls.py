from django.urls import path
from products import views

urlpatterns = [
    path('products/', views.ProductList.as_view()),
    path('stock/', views.StockListCreate.as_view()),
    path('stockrequests/', views.StockRequestListCreate.as_view()),
    path('stockrequests/<int:pk>/', views.StockRequestRetrieveUpdate.as_view()),
]
